#ifndef INCLUDED_ALL
#define INCLUDED_ALL

//******************************************************************************
//
//
//      all.h
//
//
//******************************************************************************
//------< インクルード >---------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <list>
// インクルードの追加
//1
//#include "BaseLib\\vector.h"
#include "BaseLib\\Template.h"

//2
#include "work.h"
#include "Common.h"
#include "input_manager.h"
#include "dxtk_audio.h"
#include "my_util.h"
//3
#include "MyMesh.h"
//5
#include "Helper.h"
#include "Obj3D.h"

//7
#include "Actor.h"

//8
#include "Character.h"
#include "Shot.h"
#include "Buck.h"

//9
#include "Camera.h"
#include "Judge.h"

//10
#include "Stage.h"
#include "Scene.h"

//******************************************************************************

#endif // !INCLUDED_ALL