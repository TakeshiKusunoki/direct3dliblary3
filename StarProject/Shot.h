#ifndef INCLUDED_SHOT
#define INCLUDED_SHOT

//******************************************************************************
//
//
//      Shot.h
//
//
//******************************************************************************

//==============================================================================
//
//      移動アルゴリズム
//
//==============================================================================

// 発射弾
class ShotShoot : public Actor
{
public:
	void move(OBJ3D* obj);
protected:
};
EXTERN ShotShoot shotShoot;


// 落下弾
class ShotFall : public Actor
{
public:
	void move(OBJ3D* obj);
protected:
};
EXTERN ShotFall shotFall;

////==============================================================================
////
//// 消去アルゴリズム
////
////==============================================================================
class EraceShoot : public EraseAlg
{
public:
	void erase(OBJ3D* obj); // 上と同様
};
// 消去アルゴリズムの実体
EXTERN EraceShoot eraceShoot;


class EraceShotFallTop : public EraseAlg
{
public:
	void erase(OBJ3D* obj); // 上と同様
};
EXTERN EraceShotFallTop eraceShotFallTop;


class EraceShotFallEnd : public EraseAlg
{
public:
	void erase(OBJ3D* obj); // 上と同様
};
EXTERN EraceShotFallEnd eraceShotFallEnd;


//==============================================================================
//
//      ShotManagerクラス
//
//==============================================================================
class ShotManager : public OBJ3DManager, public Singleton<ShotManager>
{
public:
	OBJ3D* addShot(MoveAlg* mvAlg,
		MoveAlgHelper* mvAlgHelper[] = nullptr,
		UINT windowNum_ = 0,
		int playerNum = ENUM::PLAYER_1,
		ENUM::SHOT_POS shotStatePos = ENUM::SHOT_POS::SHOT_TOP,//ショット位置
		DirectX::XMFLOAT3 scale = { 1,1,1 },	//大きさ
		DirectX::XMFLOAT3 angle = { 0,0,0 },	//回転角度
		DirectX::XMFLOAT3 position = { 0,0,0 },//位置
		bool isAI = true,
		DirectX::XMFLOAT4 color = { 1,1,1,1 }	//色
	); // objListに新たなOBJ3Dを追加する
};

//==============================================================================
//
//      ShotManager2クラス
//
//==============================================================================
class ShotManager2 : public OBJ3DManager, public Singleton<ShotManager2>
{
public:
	OBJ3D* addShot(MoveAlg* mvAlg,
		MoveAlgHelper* mvAlgHelper[] = nullptr,
		UINT windowNum_ = 0,
		int playerNum = ENUM::PLAYER_1,
		ENUM::SHOT_POS shotStatePos = ENUM::SHOT_POS::SHOT_TOP,//ショット位置
		DirectX::XMFLOAT3 scale = { 1,1,1 },	//大きさ
		DirectX::XMFLOAT3 angle = { 0,0,0 },	//回転角度
		DirectX::XMFLOAT3 position = { 0,0,0 },//位置
		bool isAI = true,
		DirectX::XMFLOAT4 color = { 1,1,1,1 }	//色
	); // objListに新たなOBJ3Dを追加する
};

//------< インスタンス取得 >-----------------------------------------------------
#define pShotManager  (ShotManager::getInstance())
#define SHOT_BEGIN    (pShotManager->getList()->begin())
#define SHOT_END      (pShotManager->getList()->end())


//------< インスタンス取得 >-----------------------------------------------------
#define pShotManager2  (ShotManager2::getInstance())
#define SHOT2_BEGIN    (pShotManager2->getList()->begin())
#define SHOT2_END      (pShotManager2->getList()->end())

//------< ワーク用 >-------------------------------------------------------------






//+ バリア

//==============================================================================
//
//      移動アルゴリズム
//
//==============================================================================

// 発射弾
class Balyer : public Actor
{
public:
	void move(OBJ3D* obj);
protected:
};
EXTERN Balyer balyer;

//==============================================================================
//
//      BalyertManagerクラス
//
//==============================================================================
class BalyerManager : public OBJ3DManager, public Singleton<BalyerManager>
{
public:
	void Uninit();
};
class BalyerManager2 : public OBJ3DManager, public Singleton<BalyerManager2>
{
public:
	void Uninit();
};
#define pBalyerManager  (BalyerManager::getInstance())
#define pBalyerManager2  (BalyerManager2::getInstance())


#endif // !INCLUDED_SHOT

