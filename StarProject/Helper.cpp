#include "All.h"

///////////////////////////////////
//	MoveAlg
///////////////////////////////////
//newするもの
void MoveAlgHelper::createNew(HELPER_DATA * Helper, int arrayNum)
{
}








///////////////////////////////////
//	HELPERデータ
///////////////////////////////////
HELPER_DATA::HELPER_DATA()
{
	clear();
	scale = { 1,1,1 };
	color = { 1,1,1,1 };
}

void HELPER_DATA::clear()
{
	mvAlg = nullptr;              // 移動アルゴリズム
	eraseAlg = nullptr;           // 消去アルゴリズム

	SecureZeroMemory(&Model, sizeof(Model));
	position = {};//位置
	angle = {};//回転角度
	scale = {};
	color = {};//色

	state = 0;
	timer = 0;

	// ローカル構造体---------------------------------
	Flag = { false };
}

void HELPER_DATA::moveObj(const OBJ3D* obj)
{
	if (mvAlg)mvAlg->move(this, obj);		// 移動アルゴリズムが存在すれば、moveメソッドを呼ぶ
	if (eraseAlg) eraseAlg->erace(this);	// 消去アルゴリズムが存在すれば、eraseメソッドを呼ぶ
}

void HELPER_DATA::draw(const DirectX::XMMATRIX & view, const DirectX::XMMATRIX & projection, float elapsed_time)
{
	if(Model.obj)Model.Render(view, projection, elapsed_time);
}

//void HELPER_DATA::drawHitRect(const VECTOR4 &, int i)
//{
//}

//bool HELPER_DATA::animeUpdate(int i)
//{
//	return false;
//}




//==============================================================================
//
//      移動アルゴリズム
//
//==============================================================================
void SphireMove::move(HELPER_DATA * Helper, const OBJ3D* obj)
{
	Helper; obj;
}



//==============================================================================
//
//      消去アルゴリズム
//
//==============================================================================
void EraseHelper::erace(HELPER_DATA * Helper)
{
	Helper->clear();
}












///////////////////////////////////
//	オブジェクト付随HELPER
///////////////////////////////////
void HelperManager::init()
{
	//リストのHELPERを全てクリアする
	helperList.clear();
}

void HelperManager::update(const OBJ3D* obj)
{
	// 移動
	for (auto& it : helperList)            // helperListの全ての要素をループし、itという名前で各要素にアクセス
	{
		it.moveObj(obj);                      // itのmoveメソッドを呼ぶ
	}

	// 空ノードの削除（今は気にしなくて良い）
	auto it = helperList.begin();
	while (it != helperList.end())
	{
		if (it->mvAlg)
		{
			it++;
		}
		else
		{
			it = helperList.erase(it);
		}
	}
}

void HelperManager::draw(const DirectX::XMMATRIX & view, const DirectX::XMMATRIX & projection, float elapsed_time)
{
	for (auto& it : helperList)            // objListの全ての要素をループし、itという名前で各要素にアクセス
	{
		it.draw(view, projection, elapsed_time);                      // itのdrawメソッドを呼ぶ
	}
}




void HelperManager::UnInit()
{
	for (auto& it : helperList)            //  UIListの全ての要素をループし、itという名前で各要素にアクセス
	{
		it.eraseAlg = &eraseHelper;
	}
}

//Helper追加
HELPER_DATA * HelperManager::add(MoveAlgHelper * mvAlgHelper, DirectX::XMFLOAT3 position, DirectX::XMFLOAT3 scale, DirectX::XMFLOAT3 angle, DirectX::XMFLOAT4 color)
{
	HELPER_DATA Helper;
	Helper.mvAlg = mvAlgHelper;

	Helper.position = position;
	Helper.scale = scale;
	Helper.angle = angle;
	Helper.color = color;
	helperList.push_back(Helper);//リストに追加
	return &(*helperList.rbegin());
}


