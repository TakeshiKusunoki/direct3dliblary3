#pragma once
#include "3DLib\\Skinned_mesh.h"
#include "BaseLib\\Template.h"


#include	<DirectXMath.h>

enum DIR
{
	FRONT = 1,		//	奥から手前へ
	BACK,				//	手前から奥へ
	UP,					//	下から上へ
	DOWN,				//	上から下へ
	LEFT,				//	右から左へ
	RIGHT,				//	左から右へ

	DIR_MAX
};





class Camera : public Singleton<Camera>
{
private:
	DirectX::XMVECTOR position;//位置
	DirectX::XMVECTOR target;//注視点
	DirectX::XMMATRIX projection;//	投影行列 キャッシュ用

public:
	DirectX::XMFLOAT4 cameraPos;
public:
	Camera() {};
	~Camera() {};

	DirectX::XMMATRIX SetOrthographic(float w, float h, float znear, float zfar);// 平行投影行列設定
	DirectX::XMMATRIX SetPerspective(float fov, float aspect, float znear, float zfar);//	投影行列定義
	DirectX::XMMATRIX GetView_();
	DirectX::XMMATRIX GetProjection() { return projection; }
	DirectX::XMFLOAT4 GetCameraPos() { return cameraPos; }
private:
	DirectX::XMMATRIX	GetView();
};

#define pCameraManager (Camera::getInstance())
