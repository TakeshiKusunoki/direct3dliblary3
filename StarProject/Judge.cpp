#include "All.h"



//長さ
float Length(DirectX::XMFLOAT3 v)
{
	return (v.x*v.x + v.y*v.y + v.z*v.z);
}
//当たったか
bool HitSerch(DirectX::XMFLOAT3 pos, DirectX::XMFLOAT3 v, DirectX::XMFLOAT3 pos2, DirectX::XMFLOAT3 v2)
{
	DirectX::XMFLOAT3 distanceVec = { pos2.x - pos.x, pos2.y - pos.y, pos2.z - pos.z };

	//2点間の距離がサイズより小さいなら当たっている
	return (Length(v) + Length(v2) > Length(distanceVec));
}



void Judge()
{
	//==============================================================================
	//  あたり領域の設定
	//==============================================================================
	// プレイヤーのあたり領域の設定
	// ショットの当たり判定領域
	auto& pl = *pPlayerManager->getList()->begin(); // プレイヤーの実体を参照型で取得

	auto& ene = *pEnemyManager->getList()->begin(); // プレイヤー2の実体を参照型で取得

	//敵ショットvsプレイヤー
	//for (auto& shot : *pShotManager->getList())
	//{
	//	if (shot.mvAlg != &shotFall)continue;//落下ショットのみ判定

	//	if (HitSerch(pl.position, pl.size, shot.position, shot.size))
	//	{
	//		pStageManager->point[ENUM::PLAYER_1]++;//プレイヤーにとくてん
	//		printf("プレイヤーにとくてん%d", pStageManager->point[ENUM::PLAYER_1]);
	//		shot.eraseAlg = &eraceShotFallEnd;
	//		shot.Flag.interrapt = true;
	//	}
	//}


	//敵ショットvsプレイヤー
	for (auto& shot : *pShotManager->getList())
	{
		if (shot.mvAlg != &shotFall)continue;//落下ショットのみ判定
		for (auto& bal : *pBalyerManager2->getList())
		{
			if (HitSerch(bal.position, bal.size, shot.position, shot.size))
			{
				pStageManager->point[ENUM::PLAYER_1]++;//プレイヤーにとくてん
				printf("プレイヤーにとくてん%d", pStageManager->point[ENUM::PLAYER_1]);
				shot.eraseAlg = &eraceShotFallEnd;
				shot.Flag.interrapt = true;
			}
		}
	}
	//プレイヤーショットvs敵
	//for (auto& shot : *pShotManager2->getList())
	//{
	//	if (shot.mvAlg != &shotFall)continue;

	//	if (HitSerch(ene.position, ene.size, shot.position, shot.size))
	//	{
	//		pStageManager->point[ENUM::PLAYER_2]++;//敵にとくてん
	//		printf("相手にとくてん%d", pStageManager->point[ENUM::PLAYER_2]);
	//		shot.eraseAlg = &eraceShotFallEnd;
	//		shot.Flag.interrapt = true;
	//	}
	//}

	for (auto& shot : *pShotManager2->getList())
	{
		if (shot.mvAlg != &shotFall)continue;
		for (auto& bal : *pBalyerManager->getList())
		{
			if (HitSerch(bal.position, bal.size, shot.position, shot.size))
			{
				pStageManager->point[ENUM::PLAYER_2]++;//敵にとくてん
				printf("相手にとくてん%d", pStageManager->point[ENUM::PLAYER_2]);
				shot.eraseAlg = &eraceShotFallEnd;
				shot.Flag.interrapt = true;
			}
		}
	}
}
