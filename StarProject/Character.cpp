#include "All.h"
EXTERN MyMesh* Models;
EXTERN MyMesh* ModelsCopy;

DirectX::XMFLOAT3 Location[] = {
	{ 0,0,0 },//中央
	{ 0,0,-100 },//頂上
	{ 100,0,-40 },//左
	{ -100,0,-40 },//右
	{ 70,0,80 },//左下
	{ -70,0,80 },//右下
};

//////////////////////////////
//基底クラス
/////////////////////////////
void CharBase::move(OBJ3D * obj)
{
	switch (obj->state)
	{
		case 0:
			obj->Model = Models[ENUM::MODEL_DATANUM::CUBE];//初めに読み込むモデル
			//ステート設定
			obj->actorState = pWaitState;//開始ステート
			obj->Behavier.waitState = WaitDefault;		//待機ステートで呼ばれる関数（自由に変えられる）
			obj->Behavier.walkState = WalkDefault;		//移動ステートで呼ばれる関数（自由に変えられる）
			obj->Behavier.attackState = AttckDefault;	//攻撃ステートで呼ばれる関数（自由に変えられる）
			//速度設定
			obj->Axcel._ = { KASOKU,KASOKU,KASOKU };
			obj->Axcel.speedMax = { SPEED_MAX ,SPEED_MAX ,SPEED_MAX };
			////当たり判定用サイズ
			//obj->size = {30,80,30};
			//天球上の初期位置
			obj->GameMethod.location = ENUM::STATE_POS::STATE_CENTER;
			obj->GameMethod.prevLocation = obj->GameMethod.location;

			obj->timer = 0;

			obj->state++;
		case 1:
			//関数を変える
			FuncChanger(obj);

			obj->PrevCommand = obj->Command;//1f前のコマンド取得
			obj->Command = { false };//コマンドリセット

			if (obj->Flag.AI)//AI操作か？
			{
				//ＡＩコマンド
				switch (obj->CharInfo.difficult)
				{
					case ENUM::AI::AI_EASY:
						AI_EASY(obj);
						break;
					case ENUM::AI::AI_NORMAL:
						AI_NORMAL(obj);
						break;
					case ENUM::AI::AI_HARD:
						AI_HARD(obj);
						break;
					default:
						break;
				}

			}
			else//プレイヤー操作時
			{
				//キーでキャラクターの方向を変える
				/*if (GetAsyncKeyState(VK_UP)<0)obj->Command.key._0 = true;
				if (GetAsyncKeyState(VK_RIGHT)<0)obj->Command.key._6 = true;
				if (GetAsyncKeyState(VK_DOWN)<0)obj->Command.key._12 = true;
				if (GetAsyncKeyState(VK_LEFT)<0)obj->Command.key._18 = true;*/
				// インプットしたボタンによるコマンド分岐
				if (obj->playerNum == ENUM::PLAYER_1 && obj->CharInfo.ctrl)//WaitStateではctrlは常にtrue
				{
					//プレイヤーコマンド分岐
					CommandSwitch(obj);
				}

			}


			//ステート遷移
			StateDefChange(obj);

			//ステート関数実行
			obj->actorState->update(obj);


			obj->PrevCommand = { false };//1f前のコマンドリセット
			//モデルに反映
			Relate(obj);

			obj->timer++;
			break;
	}
}





//////////////////////////////
//デフォルトキャラクター
/////////////////////////////
void CharDefault::move(OBJ3D * obj)
{
	//固有設定
	switch (obj->state)
	{
		case 0:
			//モデルアニメーション種類
			switch (obj->playerNum)
			{
				case ENUM::PLAYER_1:
					obj->Anm.Wait = Models[ENUM::MODEL_DATANUM::DANBO_TAIKI];
					obj->Anm.Attack = Models[ENUM::MODEL_DATANUM::DANBO_ATTACK];
					obj->Anm.Walk = Models[ENUM::MODEL_DATANUM::DANBO_IDOU];
					break;
				case ENUM::PLAYER_2:
					obj->Anm.Wait = ModelsCopy[ENUM::MODEL_DATANUM::DANBO_TAIKI];
					obj->Anm.Attack = ModelsCopy[ENUM::MODEL_DATANUM::DANBO_ATTACK];
					obj->Anm.Walk = ModelsCopy[ENUM::MODEL_DATANUM::DANBO_IDOU];
					break;
				default:
					obj->Anm = Models[ENUM::MODEL_DATANUM::CUBE];
					break;
			}
			break;
		default:
			break;
	}
	//共通移動アルゴリズム
	CharBase::move(obj);
}











/////////////////////////////////
//　デフォルトキャラクター２
///////////////////////////////
void CharSaico::move(OBJ3D * obj)
{
	//固有設定
	switch (obj->state)
	{
		case 0:
			//モデルアニメーション種類
			switch (obj->playerNum)
			{
				//モデルアニメーション種類
				case ENUM::PLAYER_1:
					obj->Anm.Wait = Models[ENUM::MODEL_DATANUM::PLAYER_CHARACTER];
					obj->Anm.Attack = Models[ENUM::MODEL_DATANUM::PLAYER_CHARACTER_ATK];
					obj->Anm.Walk = Models[ENUM::MODEL_DATANUM::PLAYER_CHARACTER_WALK];
					break;
				case ENUM::PLAYER_2:
					obj->Anm.Wait = ModelsCopy[ENUM::MODEL_DATANUM::PLAYER_CHARACTER];
					obj->Anm.Attack = ModelsCopy[ENUM::MODEL_DATANUM::PLAYER_CHARACTER_ATK];
					obj->Anm.Walk = ModelsCopy[ENUM::MODEL_DATANUM::PLAYER_CHARACTER_WALK];
					break;
				default:
					obj->Anm = Models[ENUM::MODEL_DATANUM::CUBE];
					break;
			}
			break;
		default:
			break;
	}
	//共通移動アルゴリズム
	CharBase::move(obj);
}

void CharSaicoRed::move(OBJ3D * obj)
{
	obj->color = {1,0,0,1};
	obj->Axcel._ = { KASOKU*0.95f,KASOKU*0.95f,KASOKU*0.95f };
	CharSaico::move(obj);
}

void CharSaicoBlue::move(OBJ3D * obj)
{
	obj->color = { 0,0,1,1 };
	obj->Axcel._ = { KASOKU*1.05f,KASOKU*1.05f,KASOKU*1.05f };
	CharSaico::move(obj);
}

void CharSaicoYellow::move(OBJ3D * obj)
{
	obj->color = { 1,1,0,1 };
	obj->Axcel._ = { KASOKU*1.1f,KASOKU*1.1f,KASOKU*1.1f };
	CharSaico::move(obj);
}
