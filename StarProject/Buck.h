#ifndef INCLUDED_BUCK
#define INCLUDED_BUCK

//******************************************************************************
//
//
//      Buck.h
//
//
//******************************************************************************

//==============================================================================
//
//      移動アルゴリズム
//
//==============================================================================

// 天球
class TenkyuMove : public MoveAlg
{
public:
	void move(OBJ3D* obj);
protected:
};
EXTERN TenkyuMove Tenkyu;

//宇宙
class UTYU : public MoveAlg
{
public:
	void move(OBJ3D* obj);
protected:
};
EXTERN UTYU Utyu;




// 惑星
class PlanetMove : public Actor
{
public:
	void move(OBJ3D* obj);
protected:
};
EXTERN PlanetMove Planet;

class PlanetMove2 : public Actor
{
public:
	void move(OBJ3D* obj);
protected:
};
EXTERN PlanetMove2 Planet2;

class PlanetMove3 : public Actor
{
public:
	void move(OBJ3D* obj);
protected:
};
EXTERN PlanetMove3 Planet3;



//==============================================================================
//
//      BuckManagerクラス
//
//==============================================================================
class BuckManager : public OBJ3DManager, public Singleton<BuckManager>
{
public:
};
class BuckManager2 : public OBJ3DManager, public Singleton<BuckManager2>
{
public:
};
class BuckManager3 : public OBJ3DManager, public Singleton<BuckManager3>
{
public:
};



//------< インスタンス取得 >-----------------------------------------------------
#define pBuckManager  (BuckManager::getInstance())
#define BUCK_BEGIN    (pBuckManager->getList()->begin())
#define BUCK_END      (pBuckManager->getList()->end())
//------< インスタンス取得 >-----------------------------------------------------
#define pBuckManager2  (BuckManager2::getInstance())
//------< インスタンス取得 >-----------------------------------------------------
#define pBuckManager3  (BuckManager3::getInstance())

//------< ワーク用 >-------------------------------------------------------------

#endif // !INCLUDED_SHOT

