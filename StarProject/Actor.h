#pragma once

//オブジェクトの行動パターン
class Actor : public MoveAlg
{
public:
	// //状態の更新
	void update(OBJ3D* obj);
public:
	// ステート関数
	static void WaitDefault(OBJ3D* obj);
	static void WalkDefault(OBJ3D* obj);
	static void AttckDefault(OBJ3D* obj);
	static void AttckBalyer(OBJ3D* obj);

protected:
	static constexpr float KASOKU = 0.2f;         // 横方向・縦方向の加速度
	static constexpr float SPEED_MAX = 5.0f;         // 横方向の最大速度
	static constexpr float SPEED_MAX_Y = 5.0f;         // 最大の落下速度(星の落ちる速さ)
	static constexpr float DANGLE = 1.0f;//1fあたりの回転角度
private:
	// 状態の変更
	static void changeState(OBJ3D* obj, ActorState* state);
protected:
	// 凡庸
	//速度反映
	static void SppedRelate(OBJ3D* obj);
	static void ShotSppedRelate(OBJ3D * obj);
	static void ShotShootSppedRelate(OBJ3D * obj);
	static void PlanetSppedRelate(OBJ3D * obj);


	//角度反映
	static void AngleRelate(OBJ3D* obj);
	//static void AngleRelateStar(OBJ3D * obj);

	// ステージの当たり判定
	//エリア範囲チェック
	static void areaCheck(OBJ3D *obj);
	static void ShotAreaCheck(OBJ3D *obj);
	static void ShotAreaCheck2(OBJ3D *obj);

protected:

	//モデルに反映
	void Relate(OBJ3D* obj);
	//AI
	void AI_EASY(OBJ3D* obj);
	void AI_NORMAL(OBJ3D* obj);
	void AI_HARD(OBJ3D* obj);
	//分岐
	void CommandSwitch(OBJ3D* obj);
	void StateDefChange(OBJ3D* obj);
	void FuncChanger(OBJ3D* obj);
};








//+ オブジェクトの状態抽象クラス
class ActorState
{
public:
	// 更新
	virtual void update(OBJ3D* obj) = 0;
};

//待機ステート
class WaitState : public ActorState, public Singleton<WaitState>
{
public:
	void update(OBJ3D* obj)
	{
		if (obj->Behavier.waitState)obj->Behavier.waitState(obj);
	}
private:
};

//移動ステート
class WalkState : public ActorState, public Singleton<WalkState>
{
public:
	void update(OBJ3D* obj)
	{
		if (obj->Behavier.walkState)obj->Behavier.walkState(obj);
	}
private:
};



//攻撃ステート
class AttackState : public ActorState, public Singleton<AttackState>
{
public:
	void update(OBJ3D* obj)
	{
		if (obj->Behavier.attackState)obj->Behavier.attackState(obj);
	}
private:
};

//クラッシュステート
class ClashState : public ActorState, public Singleton<ClashState>
{
public:
	void update(OBJ3D* obj)
	{
		if (obj->Behavier.clashState)obj->Behavier.clashState(obj);
	}
private:
};

#define pWaitState WaitState::getInstance()
#define pWalkState WalkState::getInstance()
#define pAttackState AttackState::getInstance()
#define pClashState ClashState::getInstance()

