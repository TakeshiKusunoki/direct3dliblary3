#ifndef INCLUDED_COMMON
#define	INCLUDED_COMMON

//******************************************************************************
//
//
//      common
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include <DirectXMath.h>
// 前方宣言
class ActorState;
class OBJ3D;
class HELPER_DATA;
class HelperManager;
class OBJ3DManager;

//------< //定数 >---------------------------------------------------------
// enum
namespace ENUM {
	//ＢＧＭ番号
	enum BGM_NUM : unsigned char
	{
		BGM_CERRY = 0,//ＢＧＭ桜
		BGM_END
	};

	enum TURN : unsigned char
	{
		PLAYER_TURN = 0,
		ENEMY_TURN,
	};

	//モデル番号
	enum MODEL_DATANUM : unsigned char
	{
		CUBE = 0,
		DANBO_TAIKI,
		DANBO_IDOU,
		DANBO_ATTACK,
		PLAYER_CHARACTER,//プレイヤーキャラ
		PLAYER_CHARACTER_WALK,
		PLAYER_CHARACTER_ATK,
		TENKYU1,//背景オブジェクト番号
		TENKYU2,
		WAKUSEI1,
		WAKUSEI2,
		WAKUSEI3,
		UTYU,//宇宙
		STAR,//星
		STAR_RED,//赤い星
		BARIYER1,//バリア
		BARIYER2,//バリア
	};

	// キャラクター情報
	//オブジェクト状態
	enum MOVETYPE : unsigned char
	{
		WAIT = 0,//待機
		WALK,//移動
		ATTACK,//攻撃
		DEFFENCE,
	};
	//AIの種類
	enum AI : unsigned char
	{
		AI_NONE = 255,//AIの種類
		AI_EASY = 0,//AIの種類
		AI_NORMAL,//AIの種類
		AI_HARD,//AIの種類
	};
	//天球上の位置
	enum STATE_POS : unsigned char
	{
		STATE_CENTER = 0,
		STATE_TOP,
		STATE_LEFT,
		STATE_RIGHT,
		STATE_LEFT_DOWN,
		STATE_RIGHT_DOWN,
	};

	// ショット情報
	//相手の天球上の位置
	enum SHOT_POS : unsigned char
	{
		OUT_ZONE = 255,
		SHOT_TOP = 0,
		SHOT_LEFT,
		SHOT_RIGHT,
		SHOT_LEFT_DOWN,
		SHOT_RIGHT_DOWN,
	};

	enum PLAYER_NUM :unsigned char
	{
		PLAYER_1 = 0,//プレイヤー１
		PLAYER_2 ,//プレイヤー２
		PLAYER_NONE//プレイヤーなし
	};

}

//時計の針の向き
struct JOYPAD_KEY
{
	bool _0;//時計の針の向き
	bool _3;//時計の針の向き
	bool _6;//時計の針の向き
	bool _9;//時計の針の向き
	bool _12;//時計の針の向き
	bool _15;//時計の針の向き
	bool _18;//時計の針の向き
	bool _21;//時計の針の向き
	void operator=(JOYPAD_KEY key)
	{
		this->_0 = key._0;
		this->_3 = key._3;
		this->_6 = key._6;
		this->_9 = key._9;
		this->_12 = key._12;
		this->_15 = key._15;
		this->_18 = key._18;
		this->_21 = key._21;
	}
	void operator=(bool f)
	{
		this->_0 = f;
		this->_3 = f;
		this->_6 = f;
		this->_9 = f;
		this->_12 = f;
		this->_15 = f;
		this->_18 = f;
		this->_21 = f;
	}
	bool operator==(JOYPAD_KEY key)
	{
		if (this->_0 == key._0 &&
			this->_3 == key._3 &&
			this->_6 == key._6 &&
			this->_9 == key._9 &&
			this->_12 == key._12 &&
			this->_15 == key._15 &&
			this->_18 == key._18 &&
			this->_21 == key._21
			)
		{
			return true;
		}
		return false;
	};
	bool operator!=(JOYPAD_KEY key)
	{
		if (this->_0 != key._0 ||
			this->_3 != key._3 ||
			this->_6 != key._6 ||
			this->_9 != key._9 ||
			this->_12 != key._12 ||
			this->_15 != key._15 ||
			this->_18 != key._18 ||
			this->_21 != key._21
			)
		{
			return true;
		}
		return false;
	};
};










#define SEC (60)//１秒



//------< 関数 >----------------------------------------------------------------

static float(*const ToRadian)(float degree) = DirectX::XMConvertToRadians;  // 角度をラジアンに
static float(*const ToDegree)(float radian) = DirectX::XMConvertToDegrees;  // ラジアンを角度に

#endif // !INCLUDED_COMMON