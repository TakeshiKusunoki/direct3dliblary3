#define WIN32_LEAN_AND_MEAN		// ヘッダーからあまり使われない関数を省く
#include <windows.h>
#include <assert.h>

#include "3DLib\\framework.h"
#include "BaseLib\\winMainFunc.h"
#include "BaseLib\\Template.h"
#include "Scene.h"

bool	SetFramework(framework* f);
using namespace WinFunc;

#ifdef _DEBUG
#pragma comment( lib, "./BaseLib/Lib/Debug/DirectXTK.lib" )
#pragma comment( lib, "./BaseLib/Lib/Debug/DirectXTKAudioDX.lib" )
#pragma comment( lib, "./BaseLib/Lib/Debug/DirectXTKAudioDX.lib" )
#pragma comment (lib,"libfbxsdk-md.lib")
#else
#pragma comment( lib, "./BaseLib/Lib/Release/DirectXTK.lib" )
#pragma comment( lib, "./BaseLib/Lib/Release/DirectXTKAudioDX.lib" )
#pragma comment (lib,"./BaseLib/Lib/Release/libfbxsdk-md.lib")
#endif

//=============================================================================
//      WinMain関数(ウインドウの作成)
//=============================================================================
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
{
	//〜ここに処理を書く〜
	//コンソール画面表示
	if (!Console())
	{
		assert(!"コンソール画面表示できません。");
		return (FALSE);
	}

	// ダイアログボックスの作成

	// 表示するウインドウの定義
	if (!InitWindow3(hInstance, nShowCmd))
	{
		assert(!"表示するウインドウの初期化ができません。");
		return (FALSE);
	}
	if (!InitInstance3(hInstance, nShowCmd))
	{
		assert(!"表示するウインドウの定義ができません。");
		return (FALSE);
	}
	if (!InitWindow(hInstance, nShowCmd))
	{
		assert(!"表示するウインドウの初期化ができません。");
		return (FALSE);
	}
	if (!InitInstance(hInstance, nShowCmd))
	{
		assert(!"表示するウインドウの定義ができません。");
		return (FALSE);
	}
	if (!InitWindow2(hInstance, nShowCmd))
	{
		assert(!"表示するウインドウの初期化2ができません。");
		return (FALSE);
	}
	if (!InitInstance2(hInstance, nShowCmd))
	{
		assert(!"表示するウインドウの定義2ができません。");
		return (FALSE);
	}

	// フレームワーク------------------
	//フレームワークにウインドウ情報を渡す
	HWND Hwnd[WINDOW_NUM] = { GetHwnd(Enum::WINDOW_0) , GetHwnd(Enum::WINDOW_1), GetHwnd(Enum::WINDOW_2) };
	UINT ScreenW[WINDOW_NUM] = { WinFunc::GetScreenWidth(Enum::WINDOW_0) , WinFunc::GetScreenWidth(Enum::WINDOW_1), WinFunc::GetScreenWidth(Enum::WINDOW_2) };
	UINT ScreenH[WINDOW_NUM] = { WinFunc::GetScreenHeight(Enum::WINDOW_0) ,WinFunc::GetScreenHeight(Enum::WINDOW_1), WinFunc::GetScreenHeight(Enum::WINDOW_2) };
	framework f(Hwnd, ScreenW, ScreenH);//1番目のウインドウ
	//DirectX11初期化処理
	if (!f.initialize())
	{
		assert(!"DirectX11初期化処理ができません。");
		return (FALSE);
	}
	//フレームワーク取得
	if (!SetFramework(&f))
	{
		assert(!"フレームワーク取得ができません。");
		return (FALSE);
	}
	//----------------------------


	// シーンループ
	pSceneManager->execute(pSceneInit);

	FreeConsole();
	return 0;
}
