#include "Wrapped.h"
#include	"framework.h"

static	framework*	activeFramework = nullptr;

//SetFramework
bool	SetFramework(framework* f)
{
	if (!f)	return	false;

	activeFramework = f;
	return	true;
}




// COMオブジェクト
//p_Device
ID3D11Device*		GetDevice()
{
	return	activeFramework->p_Device;
}
//p_ImidiateContext
ID3D11DeviceContext*	GetDeviceContext()
{
	return	activeFramework->p_ImidiateContext;
}

//IDXGISwapChain*
IDXGISwapChain* GetSwapChain(UINT i)
{
	return	activeFramework->p_SwapChain[i];
}

//ID3D11RenderTargetView
ID3D11RenderTargetView* GetRenderTargetView(UINT i)
{
	return	activeFramework->p_RenderTargetView[i];
}

//ID3D11DepthStencilView*
ID3D11DepthStencilView* GetDepthStencilView(UINT i)
{
	return	activeFramework->p_DepthStencilView[i];
}








//--------------------------------------------

//>SCREEN_WIDTH
LONG GetScreenWidth(UINT i)
{
	return	activeFramework->SCREEN_WIDTH[i];
}
//SCREEN_HEIGHT
LONG GetScreenHeight(UINT i)
{
	return	activeFramework->SCREEN_HEIGHT[i];
}
