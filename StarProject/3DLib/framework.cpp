#include "framework.h"
///#define PI (3.141592f)
///#define BENCH_NFLAME (8)
#define MARUTI_SAMPLING//マルチサンプリング
#define MARUTI_THREAD_//マルチスレッドフラグ
#define BENCH_MARK//ベンチマーク

bool framework::initialize()
{
	//---------------------------------------
	//01追加----------------------------------
	//---------------------------------------
	HRESULT hr = S_OK;

	// デバイス作成---------------------------------------------
	{
		UINT createDeviceFlag = 0;
#ifdef _DEBUG
		createDeviceFlag |= D3D11_CREATE_DEVICE_DEBUG;
//#elif
//		int x = 0;
//#ifndef _MARUTI_THREAD//マルチスレッドでない
//		createDeviceFlag = D3D11_CREATE_DEVICE_SINGLETHREADED;
//#endif
#endif
		//ドライバータイプ
		D3D_DRIVER_TYPE driverTypes[] = {
			D3D_DRIVER_TYPE_UNKNOWN,
			D3D_DRIVER_TYPE_HARDWARE,
			D3D_DRIVER_TYPE_REFERENCE,
			D3D_DRIVER_TYPE_WARP,
		};
		const UINT NUM_DRIVER_TYPES = ARRAYSIZE(driverTypes);//featurelevelsの配列数
		//サポートレベル
		D3D_FEATURE_LEVEL featureLevels[] =
		{
			D3D_FEATURE_LEVEL_11_0,//DirectX11のみ 9.x以降は無視
			D3D_FEATURE_LEVEL_10_1,
			D3D_FEATURE_LEVEL_10_0,
		};
		const UINT NUM_FEATURE_LEVELS = ARRAYSIZE(featureLevels);//featurelevelsの配列数


		// デバイス作成部分
		D3D_FEATURE_LEVEL featuLevelSupported;//機能レベル
		for (UINT driverTypeIndex = 0; driverTypeIndex < NUM_DRIVER_TYPES; driverTypeIndex++)
		{
			D3D_DRIVER_TYPE drvType = driverTypes[driverTypeIndex];
			hr = D3D11CreateDevice(nullptr, drvType, nullptr, createDeviceFlag, featureLevels, NUM_FEATURE_LEVELS, D3D11_SDK_VERSION,
				&p_Device, &featuLevelSupported, &p_ImidiateContext);
			if (SUCCEEDED(hr))
				break;
		}
		_ASSERT_EXPR_A(SUCCEEDED(hr), hr_trace(hr));
	}



	// デバイスチェック---------------------------------------------
	{
		//ドライバ消去処理
		hr = p_Device->GetDeviceRemovedReason();
		switch (hr)
		{
			case S_OK:
				break;
			case DXGI_ERROR_DEVICE_HUNG:
			case DXGI_ERROR_DEVICE_RESET:
			case DXGI_ERROR_DEVICE_REMOVED:
			case DXGI_ERROR_DRIVER_INTERNAL_ERROR:
			case DXGI_ERROR_INVALID_CALL:
			default:
				return false;//アプリケーションを終了
				break;
		}

		//DirectX10「コンピュータシェーダ」「未処理バッファ」「構造化バッファ」のサポート調査
		D3D11_FEATURE_DATA_D3D10_X_HARDWARE_OPTIONS isSupport;
		p_Device->CheckFeatureSupport(D3D11_FEATURE_D3D10_X_HARDWARE_OPTIONS, &isSupport, sizeof(isSupport));
		if (!isSupport.ComputeShaders_Plus_RawAndStructuredBuffers_Via_Shader_4_x)
		{
			assert(!"コンピュートシェーダができません");
			return false;//失敗
		}

		//マルチスレッドができるか？
		D3D11_FEATURE_DATA_THREADING isSupport2;
		hr = p_Device->CheckFeatureSupport(D3D11_FEATURE_THREADING, &isSupport2, sizeof(isSupport2));
		if (FAILED(hr))
		{
			assert(!"マルチスレッドができません");
			return false;
		}
	}



	// スワップチェイン作成---------------------------------------------
#ifdef MARUTI_SAMPLING
	BOOL flag_enable_4x_msaa = TRUE;
#else
	BOOL flag_enable_4x_msaa = FALSE;
#endif
	{
		//ファクトリー作成
		hr = CreateDXGIFactory(IID_PPV_ARGS(&p_Factory));
		if (FAILED(hr))
		{
			assert(!"CreateDXGIFactoryができません");
			return false;
		}

		for (int i = 0; i < WINDOW_NUM; i++)
		{
			//スワップ チェーンを記述します。
			DXGI_SWAP_CHAIN_DESC SwapChainDesc;
			ZeroMemory(&SwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));
			SwapChainDesc.BufferDesc.Width = SCREEN_WIDTH[i];
			SwapChainDesc.BufferDesc.Height = SCREEN_HEIGHT[i];
			SwapChainDesc.BufferDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
			SwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
			SwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
			SwapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
			SwapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
			SwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
			SwapChainDesc.BufferCount = 1;
			SwapChainDesc.OutputWindow = hwnd[i];
			UINT msaa_quality_level;//マルチサンプリングのクオリティレベル
			p_Device->CheckMultisampleQualityLevels(SwapChainDesc.BufferDesc.Format, 4, &msaa_quality_level);
			SwapChainDesc.SampleDesc.Count = flag_enable_4x_msaa ? 4 : 1;
			SwapChainDesc.SampleDesc.Quality = flag_enable_4x_msaa ? msaa_quality_level - 1 : 0;
			SwapChainDesc.Windowed = TRUE;
			SwapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
			SwapChainDesc.Flags = 0;

			hr = p_Factory->CreateSwapChain(p_Device, &SwapChainDesc, &p_SwapChain[i]);
			if (FAILED(hr))
			{
				assert(!"CreateSwapChainができません");
				return false;
			}


			// レンダーターゲットの作成
			D3D11_TEXTURE2D_DESC DepthDesk;//2D画面分割のデータ
			ZeroMemory(&DepthDesk, sizeof(DepthDesk));
			{
				// スワップチェインから最初のバックバッファを取得する
				ID3D11Texture2D* pBuckbuffer = nullptr;
				hr = p_SwapChain[i]->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBuckbuffer);

				_ASSERT_EXPR_A(SUCCEEDED(hr), hr_trace(hr));
				if (FAILED(hr))
				{
					assert(!"スワップチェインから最初のバックバッファを取得するができません");
					return false;
				}


				// 描画ターゲットビューの作成
				hr = p_Device->CreateRenderTargetView(pBuckbuffer, NULL, &p_RenderTargetView[i]);

				pBuckbuffer->GetDesc(&DepthDesk);
				if (pBuckbuffer)pBuckbuffer->Release();
				_ASSERT_EXPR_A(SUCCEEDED(hr), hr_trace(hr));
				if (FAILED(hr))
				{
					assert(!"描画ターゲットビューの作成ができません");
					return false;
				}
			}




			//　深度/ステンシルビューの作成
			{
				// 深度/ステンシル・テクスチャの作成
				ID3D11Texture2D* pDepthStencilTexture;
				D3D11_TEXTURE2D_DESC DepthStenstencilBufferDesc = DepthDesk;
				/*	DepthStenstencilBufferDesc.Width = SCREEN_WIDTH;
				DepthStenstencilBufferDesc.Height = SCREEN_HEIGHT;*/
				DepthStenstencilBufferDesc.MipLevels = 1;
				DepthStenstencilBufferDesc.ArraySize = 1;
				DepthStenstencilBufferDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;//DXGI_FORMAT_D32_FLOAT
																				  /*	DepthStenstencilBufferDesc.SampleDesc.Count = 1;
																				  DepthStenstencilBufferDesc.SampleDesc.Quality = 0;*/
				DepthStenstencilBufferDesc.Usage = D3D11_USAGE_DEFAULT;
				DepthStenstencilBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
				DepthStenstencilBufferDesc.CPUAccessFlags = 0;
				DepthStenstencilBufferDesc.MiscFlags = 0;
				hr = p_Device->CreateTexture2D(&DepthStenstencilBufferDesc, NULL, &pDepthStencilTexture);//作成する２Dテクスチャの設定
				_ASSERT_EXPR_A(SUCCEEDED(hr), hr_trace(hr));
				if (FAILED(hr))
				{
					assert(!"深度/ステンシル・テクスチャの作成ができません");
					return false;
				}


				//　深度/ステンシルビューの作成
				D3D11_DEPTH_STENCIL_VIEW_DESC DescDepthStencilView;
				ZeroMemory(&DescDepthStencilView, sizeof(DescDepthStencilView));
				DescDepthStencilView.Format = DepthStenstencilBufferDesc.Format;
				DescDepthStencilView.ViewDimension = flag_enable_4x_msaa ? D3D11_DSV_DIMENSION_TEXTURE2DMS : D3D11_DSV_DIMENSION_TEXTURE2D;
				DescDepthStencilView.Texture2D.MipSlice = 0;
				DescDepthStencilView.Flags = 0;
				hr = p_Device->CreateDepthStencilView(pDepthStencilTexture, &DescDepthStencilView, &p_DepthStencilView[i]);

				pDepthStencilTexture->Release();
				_ASSERT_EXPR_A(SUCCEEDED(hr), hr_trace(hr));
				if (FAILED(hr))
				{
					assert(!"深度/ステンシルビューの作成ができません");
					return false;
				}
			}
		}




	}

	/////////////////////////////////////
	////Added by Unit7
	/////////////////////////////////////
	p_Blend = new BlendMode;
	p_Blend->Initializer(p_Device);

	// 追加

	p_Blend->Set(p_ImidiateContext, BlendMode::ALPHA);

#ifdef MARUTI_THREAD
#endif

	return true;
}

//void framework::update(float elapsed_time/*Elapsed seconds from last frame*/) {
//}


//
//void framework::render(float elapsed_time/*Elapsed seconds from last frame*/) {
//
//
//
//#define VIEW_NUM 1
//	// 画面描画ターゲットの領域の設定		viewport(ビューポートの寸法を定義します。)ビューポートは -1.0〜1.0 の範囲で作られたワールド座標をスクリーン座標（表示するウインドウのサイズ）に変換するための情報)//
//	D3D11_VIEWPORT Viewport[VIEW_NUM];
//	Viewport[0].TopLeftX = 0;
//	Viewport[0].TopLeftY = 0;
//	Viewport[0].Width = static_cast<FLOAT>(SCREEN_WIDTH);
//	Viewport[0].Height = static_cast<FLOAT>(SCREEN_HEIGHT);//1lo0O10O0O8sSBloO
//	Viewport[0].MinDepth = 0.0f;
//	Viewport[0].MaxDepth = 1.0f;
//
//	// 描画ターゲット・ビューの設定
//	p_ImidiateContext->OMSetRenderTargets(VIEW_NUM, &p_RenderTargetView, p_DepthStencilView);
//	p_ImidiateContext->RSSetViewports(VIEW_NUM, Viewport);
//
//
//
//#undef VIEW_NUM
//	// 描画ターゲットのクリア
//	float ClearColor[4] = { 0.0f,0.125f,0.3f,1.0f };//クリアする値
//	p_ImidiateContext->ClearRenderTargetView(p_RenderTargetView, ClearColor);
//	// 深度ステンシル リソースをクリアします。
//	p_ImidiateContext->ClearDepthStencilView(p_DepthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
//
//
//
//
//
//
//#ifdef MARUTI_THREAD
//	for (int i = 0; i < THREAD_NUM_MAX; i++)
//#endif
//	for (int i = 0; i < 2; i++)
//	{
//
//
//		/////////////////////////////////////
//		////Added by Unit10
//		/////////////////////////////////////
//		using namespace DirectX;
//		// �@ローカル座標
//#define SCALE 1.0f
//		static DirectX::XMFLOAT4 lightVectol = { 2,2,2,0 };
//		lightVectol.y -=0.0001f;
//			XMFLOAT3 pos(0, 0.0f, 0.0f);//オブジェクトの位置
//			if (i == 0)
//			{
//				pos.x = lightVectol.x;
//				pos.y = lightVectol.y;
//				pos.z = lightVectol.z;
//			}
//		static XMFLOAT3 angle(0, 0, 0);//オブジェクトの角度
//		static XMFLOAT3 scale(SCALE, SCALE, SCALE);//オブジェクトの大きさ
//		//angle.x += XMConvertToRadians(0.05f);
//		//angle.y += XMConvertToRadians(0.05f);
//		//angle.z += XMConvertToRadians(0.05f);
//		//pos.y -= 0.1f;
//		//pos.z += 0.001f;
//		// �Aワールド座標
//		DirectX::XMMATRIX S, R, T;
//		S = DirectX::XMMatrixScaling(scale.x, scale.y, scale.z);//拡大縮小行列
//		R = DirectX::XMMatrixRotationX(angle.x);//回転行列
//		R *= DirectX::XMMatrixRotationY(angle.y);
//		R *= DirectX::XMMatrixRotationZ(angle.z);
//		T = DirectX::XMMatrixTranslation(pos.x, pos.y/*+0.2f*i*/, pos.z);//平行移動行列
//		DirectX::XMMATRIX W = S * R * T;
//
//		// �Bビュー座標
//		//static DirectX::XMFLOAT4 cameraPos = { 1,0,150,1 };
//		static DirectX::XMFLOAT4 cameraPos = { 10,5 ,-25 ,1 };
//		static DirectX::XMFLOAT4 cameraTarget = { 0,0,0,1 };
//		const DirectX::XMFLOAT4 normal = { 0,1,0,0 };//上方向[
//		//cameraPos.x += 0.01f;
//		//cameraPos.y -= 0.01f;
//		//cameraPos.z += 0.1f;
//		XMVECTOR eye = DirectX::XMLoadFloat4(&cameraPos);
//		XMVECTOR focus = DirectX::XMLoadFloat4(&cameraTarget);
//		XMVECTOR up = DirectX::XMLoadFloat4(&normal);
//		XMMATRIX viewMatrix = XMMatrixLookAtLH(eye, focus, up);
//
//		// �Cプロジェクション座標
//		static float fovY = DirectX::XMConvertToRadians(30.0f);//(カメラの視野角)
//		float    aspect = (float)SCREEN_W / (float)SCREEN_H;
//		float    nearZ = 0.1f;
//		float    farZ = 400.0f;
//		XMMATRIX projMatrix = XMMatrixPerspectiveFovLH(fovY, aspect, nearZ, farZ);//透視投影行列
//
//		// �Dワールド・ビュー・プロジェクション行列（WVP）を合成する
//		XMFLOAT4X4 WVP, w;
//		DirectX::XMStoreFloat4x4(&WVP, W * viewMatrix * projMatrix);
//		DirectX::XMStoreFloat4x4(&w, W);
//
//		// �Eライト方向・材質色を設定する
//
//		static DirectX::XMFLOAT4 materialColor = { 1,1,1,1 };
//
//		// 描画
//#define CAMERA_MOVE_SPEED 0.05f
//		if (GetAsyncKeyState(VK_DOWN) < 0)
//		{
//			cameraPos.y += CAMERA_MOVE_SPEED;
//		}
//		if (GetAsyncKeyState(VK_UP) < 0)
//		{
//			cameraPos.y -= CAMERA_MOVE_SPEED;
//		}
//		if (GetAsyncKeyState(VK_LEFT) < 0)
//		{
//			cameraPos.x += CAMERA_MOVE_SPEED;
//		}
//		if (GetAsyncKeyState(VK_RIGHT) < 0)
//		{
//			cameraPos.x -= CAMERA_MOVE_SPEED;
//		}
//		if (GetAsyncKeyState('W') < 0)
//		{
//			cameraPos.z += CAMERA_MOVE_SPEED;
//		}
//		if (GetAsyncKeyState('S') < 0)
//		{
//			cameraPos.z -= CAMERA_MOVE_SPEED;
//		}
//		bool f = true;
//		if (GetAsyncKeyState('Q') < 0)
//		{
//			f = false;
//		}
//		if(i==0)
//		cube->render(p_ImidiateContext, WVP, w, lightVectol, materialColor, true);
//		//mesh->render(p_DeviceContext, WVP, w, lightVectol, materialColor, f);
//		//mesh->ThreadFunc(&pCommandList, pDeferredContext, WVP, w, lightVectol, materialColor, f);
//#ifdef MARUTI_THREAD
//		if(i==0)
//#endif
//			if (i > 0)
//		//skin[0]->render(p_ImidiateContext, WVP, w, lightVectol, materialColor, cameraPos, true, elapsed_time);
//		//skin[1]->render(p_DeviceContext, WVP, w, lightVectol, materialColor, true);
//		/// レンダリングされたイメージをユーザーに表示します。
//#ifdef MARUTI_THREAD
//
//		{
//			ID3D11DeviceContext* pDeferredContext = NULL;
//			ID3D11CommandList* pCommandList = NULL;
//			if (!pDeferredContext)p_Device->CreateDeferredContext(0, &pDeferredContext);
//			ID3D11RenderTargetView* rtv[1] = { p_RenderTargetView };
//			if (pDeferredContext) {
//				pDeferredContext->OMSetRenderTargets(1, rtv, p_DepthStencilView);
//				pDeferredContext->RSSetViewports(1, Viewport);
//			}
//			if(!pCommandList)mesh[i]->ThreadFunc(&pCommandList, pDeferredContext, WVP, w, lightVectol, materialColor, f);
//			if (pDeferredContext) {
//				pDeferredContext->ClearState();
//				pDeferredContext->Release();
//			}
//			if (pCommandList) {
//				//p_ImidiateContext->ExecuteCommandList(pCommandList, FALSE);
//				pCommandList->Release();
//			}
//		}
//#else
//		//for (int i = 0; i < NUM; i++)
//		{
//
//			//mesh[0]->render(p_ImidiateContext, WVP, w, lightVectol, materialColor, f);
//		}
//#endif
//	}
//
//
//	return;
//}
//
//// 無駄な画面描画の抑制
//bool framework::StanbyPresant(float elapsed_time)
//{
//	static bool standByMode = false;//スタンバイ状態を管理するstatic変数
//	HRESULT hr = S_OK;
//	// スタンバイモード
//	if (standByMode)
//	{
//		hr = p_SwapChain->Present(0, DXGI_PRESENT_TEST);
//		if (hr == DXGI_STATUS_OCCLUDED)// 描画する必要がない(表示領域がない)
//		{
//			return standByMode;
//		}
//		standByMode = false;//スタンバイ・モードを解除
//	}
//	// 画面の更新
//	render(elapsed_time);
//	// レンダリングされたイメージをユーザーに表示します。
//	hr = p_SwapChain->Present(0, 0);
//	if (hr == DXGI_STATUS_OCCLUDED)// 描画する必要がない(表示領域がない)
//	{
//		standByMode = true;//スタンバイ・モードに入る
//	}
//	return standByMode;
//}
//
////マルチスレッドのコマンド格納
//void framework::ThreadFunc(Static_mesh ** mesh, ID3D11CommandList ** ppCommandList, ID3D11DeviceContext * pDeferredContext,
//	const DirectX::XMFLOAT4X4 & wvp, const DirectX::XMFLOAT4X4 & world, const DirectX::XMFLOAT4 & lightVector, const DirectX::XMFLOAT4 * materialColor, bool FlagPaint)
//{
//
//}
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
////+ RUN部分---------------------------------------------------------------------------
//int framework::run()
//{
////	MSG msg = {};
////
////	if (!initialize()) return 0;
////
////	 時間計測準備
////	LARGE_INTEGER LargeInteger;
////	QueryPerformanceFrequency(&LargeInteger);
////	CountsPerSecond = LargeInteger.QuadPart;
////	CountsPerFrame = CountsPerSecond / 60;
////	 時間計測開始
////	QueryPerformanceCounter(&LargeInteger);
////	NextCounts = LargeInteger.QuadPart;
////
////	while (WM_QUIT != msg.message)
////	{
////		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
////		{
////			TranslateMessage(&msg);
////			DispatchMessage(&msg);
////		}
////		else
////		{
////			// フレーム制御
////			LARGE_INTEGER count;
////			QueryPerformanceCounter(&count);
////#ifndef BENCH_MARK
////			if (NextCounts > count.QuadPart)
////			{
////				DWORD t = (DWORD)((NextCounts - count.QuadPart) * 1000 / CountsPerSecond);
////				Sleep(t);
////				continue;
////			}
////#endif
////			timer.tick();
////			calculate_frame_stats();
////			update(timer.time_interval());
////			render(timer.time_interval());
////			StanbyPresant(timer.time_interval());
////
////			 次の活動予定時間
////			NextCounts += CountsPerFrame;
////		}
////	}
////	return static_cast<int>(msg.wParam);
//	return 0;
//}
