#pragma once
//#include  <string>
#include <d3d11.h>
// 画像参照
class ResourceManager {
private:
	static const int RESOURCE_MAX = 64;
	//? ローカル構造体
	struct RESOURCE_INFO {
		// 変数
		wchar_t fileName[256];
		int counter;//読み込み数カウンタ
		ID3D11ShaderResourceView* p_ShaderResourceView;//シェーダーリソースビューオブジェクト(ID3D11ShaderResoyrceView)の作成
													   // RESOURCE_INFOの変数を要素に持つ、ローカル関数
													   //? ローカル関数のコンストラクタ(class ResourceManagerからしか見えない)
		RESOURCE_INFO() :counter(0), p_ShaderResourceView(NULL) {
			fileName[0] = '\0';
		}
		//画像が消えたらcounterを減らす//画像をデリートする関数
		void Release(bool bForce = false) {
			//もともと参照されてる数が０
			if (counter == 0) {
				return;
			}
			//画像が0以下
			if (--counter <= 0) {
				bForce = true;
			}
			//画像破棄処理
			if (bForce) {
				if (p_ShaderResourceView)p_ShaderResourceView->Release();
				p_ShaderResourceView = NULL;
				counter = 0;
				fileName[0] = '\0';
			}
		}
	};

	struct RESOURCE_VERTEXSHADER {
		// 変数
		wchar_t fileName[256];
		int counter;//読み込み数カウンタ
		ID3D11VertexShader* p_VertexShader;//シェーダーリソースビューオブジェクト(ID3D11ShaderResoyrceView)の作成
		ID3D11InputLayout* p_InputLayout;
		//? ローカル関数のコンストラクタ(class ResourceManagerからしか見えない)
		RESOURCE_VERTEXSHADER() :counter(0), p_VertexShader(NULL), p_InputLayout(NULL) {
			fileName[0] = '\0';
		}
		//画像が消えたらcounterを減らす//画像をデリートする関数
		void Release(bool bForce = false) {
			//もともと参照されてる数が０
			if (counter == 0) {
				return;
			}
			//画像が0以下
			if (--counter <= 0) {
				bForce = true;
			}
			//画像破棄処理
			if (bForce) {
				if (p_VertexShader)p_VertexShader->Release();
				if (p_InputLayout)p_InputLayout->Release();
				p_VertexShader = NULL;
				counter = 0;
				fileName[0] = '\0';
			}
		}
	};

	struct RESOURCE_PIXELSHADER {
		// 変数
		wchar_t fileName[256];
		int counter;//読み込み数カウンタ
		ID3D11PixelShader* p_PixelShader;//シェーダーリソースビューオブジェクト(ID3D11ShaderResoyrceView)の作成
										 //? ローカル関数のコンストラクタ(class ResourceManagerからしか見えない)
		RESOURCE_PIXELSHADER() :counter(0), p_PixelShader(NULL) {
			fileName[0] = '\0';
		}
		//画像が消えたらcounterを減らす//画像をデリートする関数
		void Release(bool bForce = false) {
			//もともと参照されてる数が０
			if (counter == 0) {
				return;
			}
			//画像が0以下
			if (--counter <= 0) {
				bForce = true;
			}
			//画像破棄処理
			if (bForce) {
				if (p_PixelShader)p_PixelShader->Release();
				p_PixelShader = NULL;
				counter = 0;
				fileName[0] = '\0';
			}
		}
	};

	static RESOURCE_INFO ResourceInfo[RESOURCE_MAX];
	static RESOURCE_VERTEXSHADER ResourceVertexShader[RESOURCE_MAX];
	static RESOURCE_PIXELSHADER ResourcePixcelShader[RESOURCE_MAX];

public:
	ResourceManager();
	~ResourceManager();
	void Release();
	bool LoadShaderResourceView(ID3D11Device*, const wchar_t*, ID3D11ShaderResourceView**, D3D11_TEXTURE2D_DESC*);
	bool LoadVertexShader(ID3D11Device*, const char*, D3D11_INPUT_ELEMENT_DESC*, const UINT, ID3D11VertexShader**, ID3D11InputLayout**);
	bool LoadPixelShader(ID3D11Device*, const char*, ID3D11PixelShader**);
	void ReleaseShaderResourceView(ID3D11ShaderResourceView*);
	void ReleaseVertexShader(ID3D11VertexShader*, ID3D11InputLayout*);
	void ReleasePixelShader(ID3D11PixelShader*);
};
