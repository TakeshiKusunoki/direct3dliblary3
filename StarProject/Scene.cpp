#include "3DLib\\Wrapped.h"
#include "All.h"
#include <assert.h>
#include <mmsystem.h>

#define BENCH_MARK_


EXTERN const char* modelFileName[];//最後にNULL//fbxモデルのファイルパス
EXTERN const char* constModelFileName[];//最後にNULL//fbxモデルのファイルパス
MyMesh* Models;//モデルデータ
MyMesh* ModelsCopy;//モデルデータ複製

bool Scene::standByMode = false;
high_resolution_timer* IScene::Timer[WINDOW_NUM] = { WinFunc::GetElapsedTimer(),nullptr,nullptr };

//////////////////////////////
//シーン
IScene * Scene::execute()
{
	pNext = nullptr;

	for (int i = 0; i <WINDOW_NUM; i++)
	{
		init(i);		//初期設定
		p_RenderTargetView[i] = GetRenderTargetView(i);
		p_DepthStencilView[i] = GetDepthStencilView(i);
	}
				// メインループ
	// 時間計測準備
		LARGE_INTEGER LargeInteger;

	QueryPerformanceFrequency(&LargeInteger);
	CountsPerSecond = LargeInteger.QuadPart;
	CountsPerFrame = CountsPerSecond / 60;
	//PlaySound(L"DATA\\MUSIC\\Tirry.wav", NULL, SND_FILENAME | SND_ASYNC);
	pMusic->musicPlay(ENUM::BGM_CERRY, true);

	// 時間計測開始
	QueryPerformanceCounter(&LargeInteger);
	NextCounts = LargeInteger.QuadPart;


	while (WinFunc::MsgLoop())
	{

		// フレーム制御
		LARGE_INTEGER count;
		QueryPerformanceCounter(&count);
#ifndef BENCH_MARK
		if (NextCounts > count.QuadPart)
		{
			DWORD t = (DWORD)((NextCounts - count.QuadPart) * 1000 / CountsPerSecond);
			Sleep(t);
			continue;
		}
#endif
		//タイマー取得
		//for (UINT i = 0; i < WINDOW_NUM; i++)
		{
			Timer[0]->tick();
			Calculate_frame_stats(0);
		}

		pInputManager->update(WinFunc::GetHwnd(0));
		// 描画処理
		for (UINT i = 0; i < WINDOW_NUM; i++)
		{
			// 入力処理

			// 更新
			update(i);
			StanbyPresant(i);
		}

		//次の活動予定時間
		NextCounts += CountsPerFrame;

		if (pNext)break;
	}
	uninit();

	return pNext;
}




bool Scene::StanbyPresant(UINT i)
{
	if (i > WINDOW_NUM)
	{
		assert(!"StanbyPresant i num many");
		return false;
	}
	HRESULT hr = S_OK;
	// スタンバイモード
	if (standByMode)
	{
		hr = GetSwapChain(i)->Present(0, DXGI_PRESENT_TEST);
		if (hr == DXGI_STATUS_OCCLUDED)// 描画する必要がない(表示領域がない)
		{
			return standByMode;
		}
		standByMode = false;//スタンバイ・モードを解除
	}

	// 画面の更新
	{
#define VIEW_NUM 1
		// 画面描画ターゲットの領域の設定		viewport(ビューポートの寸法を定義します。)ビューポートは -1.0〜1.0 の範囲で作られたワールド座標をスクリーン座標（表示するウインドウのサイズ）に変換するための情報)//
		D3D11_VIEWPORT Viewport[VIEW_NUM];
		Viewport[0].TopLeftX = 0;
		Viewport[0].TopLeftY = 0;
		Viewport[0].Width = static_cast<FLOAT>(WinFunc::GetScreenWidth(i));
		Viewport[0].Height = static_cast<FLOAT>(WinFunc::GetScreenHeight(i));//1lo0O10O0O8sSBloO
		Viewport[0].MinDepth = 0.0f;
		Viewport[0].MaxDepth = 1.0f;

		// 描画ターゲット・ビューの設定
		GetDeviceContext()->OMSetRenderTargets(VIEW_NUM, &p_RenderTargetView[i], p_DepthStencilView[i]);
		GetDeviceContext()->RSSetViewports(VIEW_NUM, Viewport);



#undef VIEW_NUM
		// 描画ターゲットのクリア
		float ClearColor[4] = { 8.0f,8.125f,8.3f,1.0f };//クリアする値
		//if (i == Enum::WINDOW_2)
		//{
		//	ClearColor[0] = { 0.0f };//クリアする値
		//	ClearColor[1] = { 7.125f };//クリアする値
		//	ClearColor[2] = { 6.3f};//クリアする値
		//	ClearColor[3] = { 1.0f };//クリアする値
		//}
		GetDeviceContext()->ClearRenderTargetView(p_RenderTargetView[i], ClearColor);
		// 深度ステンシル リソースをクリアします。
		GetDeviceContext()->ClearDepthStencilView(p_DepthStencilView[i], D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);

	}
	draw(i);
	// レンダリングされたイメージをユーザーに表示します。
	hr = GetSwapChain(i)->Present(0, 0);
	if (hr == DXGI_STATUS_OCCLUDED)// 描画する必要がない(表示領域がない)
	{
		standByMode = true;//スタンバイ・モードに入る
	}
	return standByMode;
}



// fps表示
void  Scene::Calculate_frame_stats(UINT i)
{
	if (i >= WINDOW_NUM)
	{
		assert(!"引数を画面の数に収めてください");
	}
	// Code computes the average frames per second, and also the
	// average time it takes to render one frame.  These stats
	// are appended to the window caption bar.
	static int frames[WINDOW_NUM] = { 0 };
	static float time_tlapsed[WINDOW_NUM] = { 0.0f };

	frames[i]++;

	// Compute averages over one second period.
	if ((Timer[i]->time_stamp() - time_tlapsed[i]) >= 1.0f)
	{
		float fps = static_cast<float>(frames[i]); // fps = frameCnt / 1
		float mspf = 1000.0f / fps;
		std::ostringstream outs;
		outs.precision(6);
		outs << "FPS : " << fps << " / " << "Frame Time : " << mspf << " (ms)";
		SetWindowTextA(WinFunc::GetHwnd(i), outs.str().c_str());

		// Reset for next average.
		frames[i] = 0;
		time_tlapsed[i] += 1.0f;
	}
}











//////////////////////////////////////
// シーンマネージャー
void SceneManager::execute(IScene* pScene)
{
	while (pScene)
	{
		// 更新処理
		pScene = pScene->execute();
	}

	// 終了処理-----------------------------------
	//モデルをデリート
	if (Models)
	{
		UINT modelNum = 0;
		for (modelNum = 0; modelFileName[modelNum] != nullptr; modelNum++)
		{
			Models[modelNum].Release();
		}
		for (UINT i = 0; constModelFileName[i] != nullptr; i++)
		{
			Models[i + modelNum].Release();
		}
		delete[] Models;
	}
	if (ModelsCopy)
	{
		UINT modelNum = 0;
		for (modelNum = 0; modelFileName[modelNum] != nullptr; modelNum++)
		{
			ModelsCopy[modelNum].Release();
		}
		delete[] ModelsCopy;
	}

	//フォント破棄
	Font::ReleaseImage();

	//音楽データ破棄
	for (size_t i = 0; i <ENUM::BGM_END; i++)
	{
		pMusic->musicUnload(i);
	}

	WinFunc::Uninit();
}





//３Ｄモデルのロード
void loadModel()
{

	UINT modelNum;//modelFileNameの数
	//モデルの数を調べる
	for (modelNum = 0; modelFileName[modelNum] != nullptr; modelNum++)
	{
	}
	//オブジェクトコピー生成
	if (modelNum != 0)ModelsCopy = new MyMesh[modelNum];

	UINT modelNum2 = modelNum;//modelFileNameの数 + constModelFileNameの数
	for ( UINT i = 0; constModelFileName[i] != nullptr; i++, modelNum2++)
	{
	}

	//モデルの数分MyMesh オブジェクト生成
	if (modelNum2 != 0)Models = new MyMesh[modelNum2];
	for (UINT i = 0; i < modelNum; i++)
	{
		Models[i].Load(modelFileName[i]);
		printf("モデルロード・・%d", i);
		ModelsCopy[i].Load(modelFileName[i]);
		printf("モデルコピーロード・・%d\n", i);
	}
	//モデルロード
	for (UINT i = 0; i < modelNum2 - modelNum; i++)
	{
		Models[i + modelNum].Load(constModelFileName[i]);
		printf("モデルロード・・%d\n", i + modelNum);
	}


}




////////////////////////////////////
//　シーン初期化
IScene* SceneInit::execute()
{
	// ゲーム初期設定
	//乱数取得
	srand(static_cast<unsigned int>(time(nullptr)));




	printf("ロード中・・\n");
	loadModel();
	printf("ロード完了・・\n");

	//音楽ロード
	pMusic->musicLoad(ENUM::BGM_CERRY, L"DATA\\MUSIC\\Tirry.wav", 1);

	//フォントロード
	Font::LoadImage(GetDevice(), L"fonts\\font2.png");
	Font::SetColor(1, 1, 1, 1);


	//pMusic->soundLoad();
	pInputManager->init();
	return pSceneTitle;
}
