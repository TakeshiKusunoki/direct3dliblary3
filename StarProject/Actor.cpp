#include "All.h"

//天球上の位置座標
EXTERN DirectX::XMFLOAT3 Location[];

// //状態の更新
void Actor::update(OBJ3D* obj)
{
	// ActorStateの呼び出し
	if (obj->actorState != nullptr)obj->actorState->update(obj);
}




// 状態の変更
//stateNum : ステートの番号
void Actor::changeState(OBJ3D* obj, ActorState* state)
{
	obj->actorState = state;
}
// -------------------------------------------------------------------------------------------------
















//////////////////////////////////////////////
//+ ステイトメソッド
//////////////////////////////////////////////

////////////////////////////////////////////////
//+ キャラで共用
// 待機ステート
void Actor::WaitDefault(OBJ3D* obj)
{
//#define RECT5 108//5角形１つの角度
	switch (obj->stateMethodWait.state)
	{
		case 0://waitステート初期化
			obj->Model = obj->Anm.Wait;//waitステートでのモデルデータ
			obj->CharInfo.ctrl = true;//AI用コマンド受付フラグ
			//printf(" obj->GameMethod.location%d\n", obj->GameMethod.location);

			obj->stateMethodWait.state++;
		case 1://処理部分
			// 振り向き
			AngleRelate(obj);

			obj->stateMethodWait.timer++;
			break;
	}
}






// 移動すてーと
void Actor::WalkDefault(OBJ3D * obj)
{
	switch (obj->stateMethodWalk.state)
	{
		case 0://移動ステート初期化
			obj->Model = obj->Anm.Walk;
			obj->speed = { 0,0,0 };
			obj->CharInfo.ctrl = false;

			//printf("obj->GameMethod.location	%d\n", obj->GameMethod.location);
			//printf("obj->angle.x	:%f	obj->angle.y	:%f	obj->angle.z	:%f\n", obj->angle.x, obj->angle.y, obj->angle.z);//debug
			obj->stateMethodWalk.state++;
		case 1://処理部分

			// 移動
			obj->speed.x += obj->Axcel._.x;
			obj->speed.z += obj->Axcel._.z;




			// 凡庸移動
			SppedRelate(obj);
			// エリア範囲チェック
			//areaCheck(obj);

			obj->stateMethodWalk.timer++;
			break;
	}

}






// 攻撃ステート
void Actor::AttckDefault(OBJ3D * obj)
{
	switch (obj->stateMethodAttack.state)
	{
		case 0://アタックステート初期化
			obj->Model = obj->Anm.Attack;
			obj->CharInfo.ctrl = false;
			//obj->stateMethodAttack.isShoot = false;

			obj->stateMethodAttack.state++;
		case 1://処理部分
			 //アニメーションの終わり
			if (obj->Model.GetFlagBoneAnimationEnd())
			{
				//攻撃していたら星が出ない
				if (!obj->Flag.hadAttacked)
				{
					obj->Flag.hadAttacked = true;
					MoveAlgHelper* moveAlgHelper[1] = { nullptr };
					switch (obj->playerNum)
					{
						case ENUM::PLAYER_1:
							pShotManager->addShot(&shotShoot, moveAlgHelper, obj->windowNum, obj->playerNum, obj->ShotInfo.statePos, DirectX::XMFLOAT3(2, 2, 2), DirectX::XMFLOAT3(0, 90, 0), DirectX::XMFLOAT3(obj->position.x, obj->position.y + 100, obj->position.z), false);

							break;
						case ENUM::PLAYER_2:
							pShotManager2->addShot(&shotShoot, moveAlgHelper, obj->windowNum, obj->playerNum, obj->ShotInfo.statePos, DirectX::XMFLOAT3(2, 2, 2), DirectX::XMFLOAT3(0, 90, 0), DirectX::XMFLOAT3(obj->position.x, obj->position.y + 100, obj->position.z), false);
						default:
							break;
					}
				}
				//弾がでた
				//obj->stateMethodAttack.isShoot = true;
			}
			obj->stateMethodAttack.timer++;
			break;
	}
}


//バリア
void Actor::AttckBalyer(OBJ3D * obj)
{
	switch (obj->stateMethodAttack.state)
	{
		case 0://アタックステート初期化
			obj->Model = obj->Anm.Attack;
			obj->CharInfo.ctrl = false;
			switch (obj->playerNum)
			{
				case ENUM::PLAYER_1:
					{
						MoveAlgHelper* moveAlgHelper[1] = { nullptr };
						pBalyerManager->add(&balyer, moveAlgHelper, DirectX::XMFLOAT3(obj->position.x, obj->position.y + 50, obj->position.z), obj->windowNum, obj->playerNum);
					}
					break;
				case ENUM::PLAYER_2:
					{
						MoveAlgHelper* moveAlgHelper[1] = { nullptr };
						pBalyerManager2->add(&balyer, moveAlgHelper, DirectX::XMFLOAT3(obj->position.x, obj->position.y + 50, obj->position.z), obj->windowNum, obj->playerNum);
					}
					break;
				default:
					break;
			}


			obj->stateMethodAttack.state++;
		case 1://処理部分

			obj->stateMethodAttack.timer++;
			break;
	}
}




















//////////////////////////////////////////////
//+ 組み込みメソッド
//////////////////////////////////////////////
// 凡庸移動
void Actor::SppedRelate(OBJ3D * obj)
{

	// 最大速度チェック
	obj->speed.x = clamp(obj->speed.x, -obj->Axcel.speedMax.x, obj->Axcel.speedMax.x);
	obj->speed.y = clamp(obj->speed.y, -obj->Axcel.speedMax.y, obj->Axcel.speedMax.y);
	obj->speed.z = clamp(obj->speed.z, -obj->Axcel.speedMax.z, obj->Axcel.speedMax.z);

	// X平面移動
	obj->position.x += sinf(obj->angle.y)*obj->speed.x;
	obj->position.z += cosf(obj->angle.y)*obj->speed.z;

	// Y軸移動
	//obj->position.y += sinf(obj->angle.y)*obj->speed.y;

}




// 凡庸移動
void Actor::ShotSppedRelate(OBJ3D * obj)
{

	// 最大速度チェック
	obj->speed.x = clamp(obj->speed.x, -obj->Axcel.speedMax.x, obj->Axcel.speedMax.x);
	obj->speed.y = clamp(obj->speed.y, -obj->Axcel.speedMax.y, obj->Axcel.speedMax.y);
	obj->speed.z = clamp(obj->speed.z, -obj->Axcel.speedMax.z, obj->Axcel.speedMax.z);

	// X平面移動
	obj->position.x += obj->speed.x;
	obj->position.z += obj->speed.z;

	// Y軸移動
	obj->position.y += obj->speed.y;

	// X平面移動
	obj->position.x += sinf(obj->angle.y -ToRadian(180) )*obj->speed.x;
	obj->position.z += cosf(obj->angle.y - ToRadian(180))*obj->speed.z;

	// Y軸移動
	//obj->position.y += sinf(obj->angle.y)*obj->speed.y;

}

// 凡庸移動
void Actor::ShotShootSppedRelate(OBJ3D * obj)
{

	// 最大速度チェック
	obj->speed.x = clamp(obj->speed.x, -obj->Axcel.speedMax.x, obj->Axcel.speedMax.x);
	obj->speed.y = clamp(obj->speed.y, -obj->Axcel.speedMax.y, obj->Axcel.speedMax.y);
	obj->speed.z = clamp(obj->speed.z, -obj->Axcel.speedMax.z, obj->Axcel.speedMax.z);

	// X平面移動
	obj->position.x += obj->speed.x;
	obj->position.z += obj->speed.z;

	// Y軸移動
	obj->position.y += obj->speed.y;

	// X平面移動
	//obj->position.x += sinf(obj->angle.y - ToRadian(180))*obj->speed.x;
	//obj->position.z += cosf(obj->angle.y - ToRadian(180))*obj->speed.z;

	// Y軸移動
	obj->position.y += obj->speed.y;

}

// 凡庸移動
void Actor::PlanetSppedRelate(OBJ3D * obj)
{

	// 最大速度チェック
	obj->speed.x = clamp(obj->speed.x, -obj->Axcel.speedMax.x, obj->Axcel.speedMax.x);
	obj->speed.y = clamp(obj->speed.y, -obj->Axcel.speedMax.y, obj->Axcel.speedMax.y);
	obj->speed.z = clamp(obj->speed.z, -obj->Axcel.speedMax.z, obj->Axcel.speedMax.z);

	// X平面移動
	obj->position.x += sinf(obj->angle.x)*obj->speed.x;
	obj->position.z += cosf(obj->angle.z)*obj->speed.z;

	// Y軸移動
	obj->position.y += sinf(obj->angle.y)*obj->speed.y;

}


//凡庸振り向き
void Actor::AngleRelate(OBJ3D * obj)
{
	 //振り向き
	if (obj->Command.stick._0 || obj->Command.key._0)
	{
		obj->angle.y = ToRadian(180.0f);
	}
	if (obj->Command.stick._3 || obj->Command.key._3)
	{
		obj->angle.y = ToRadian(225.0f);
	}
	if (obj->Command.stick._6 || obj->Command.key._6)
	{
		obj->angle.y = ToRadian(270.0f);
	}
	if (obj->Command.stick._9 || obj->Command.key._9)
	{
		obj->angle.y = ToRadian(315.0f);
	}
	if (obj->Command.stick._12 || obj->Command.key._12)
	{
		obj->angle.y = ToRadian(0.0f);
	}
	if (obj->Command.stick._15 || obj->Command.key._15)
	{
		obj->angle.y = ToRadian(45.0f);
	}
	if (obj->Command.stick._18 || obj->Command.key._18)
	{
		obj->angle.y = ToRadian(90.0f);
	}
	if (obj->Command.stick._21 || obj->Command.key._21)
	{
		obj->angle.y = ToRadian(135.0f);
	}

}

// 凡庸振り向き
void AngleRelateStar(OBJ3D * obj)
{
	float ang5 = (108 >> 1);//５角形角度
	float cen5 = (72.0f);//５角形中心角度
	// 振り向き
	if (obj->GameMethod.direction._0)//上方向
	{
		obj->angle.y = ToRadian(180.0f);
	}
	if (obj->GameMethod.direction._6)//右
	{
		obj->angle.y = ToRadian(270.0f);
	}
	if (obj->GameMethod.direction._12)//下
	{
		obj->angle.y = ToRadian(0.0f);
	}
	if (obj->GameMethod.direction._18)//左
	{
		obj->angle.y = ToRadian(90.0f);
	}
	//振り向き
	switch (obj->GameMethod.location)
	{
		case ENUM::STATE_CENTER:
			if (obj->GameMethod.direction._3)//右上
			{
				obj->angle.y = ToRadian(180.0f + cen5);
			}
			if (obj->GameMethod.direction._9)//右下
			{
				obj->angle.y = ToRadian(180.0f + cen5 * 2);
			}
			if (obj->GameMethod.direction._15)//左下
			{
				obj->angle.y = ToRadian(180.0f + cen5 * 3);
			}
			if (obj->GameMethod.direction._21)//左上
			{
				obj->angle.y = ToRadian(180.0f + cen5 * 4);
			}
			break;
		case ENUM::STATE_TOP:
			if (obj->GameMethod.direction._9)//右下
			{
				obj->angle.y = ToRadian(270.0f + 36);
			}
			if (obj->GameMethod.direction._15)//左下
			{
				obj->angle.y = ToRadian(90.0f - 36);//198°
			}
			break;
		case ENUM::STATE_RIGHT:	case ENUM::STATE_LEFT:
			if (obj->GameMethod.direction._3)//右上
			{
				obj->angle.y = ToRadian(180.0f + 54);
			}
			if (obj->GameMethod.direction._6)//右(中央に戻る)
			{
				obj->angle.y = ToRadian(180.0f + 108.0f);
			}
			if (obj->GameMethod.direction._9)//右下
			{
				obj->angle.y = ToRadian(0.0f - 18);
			}
			if (obj->GameMethod.direction._15)//左下
			{
				obj->angle.y = ToRadian(0.0f + 18);//198°
			}
			if (obj->GameMethod.direction._18)//左(中央に戻る)
			{
				obj->angle.y = ToRadian(90.0f - 28.0f);
			}
			if (obj->GameMethod.direction._21)//左上
			{
				obj->angle.y = ToRadian(180.0f - 54);
			}
			break;
		case ENUM::STATE_LEFT_DOWN:
			if (obj->GameMethod.direction._3)//右上
			{
				obj->angle.y = ToRadian(180.0f + 36);
			}
			if (obj->GameMethod.direction._21)//左上
			{
				obj->angle.y = ToRadian(180.0f - 18);
			}
			break;
		case ENUM::STATE_RIGHT_DOWN:
			if (obj->GameMethod.direction._3)//右上
			{
				obj->angle.y = ToRadian(180.0f + 18);
			}
			if (obj->GameMethod.direction._21)//左上
			{
				obj->angle.y = ToRadian(180.0f - 36);
			}
			break;
		default:
			break;
	}
	//if (moveFlag)
	/*{
		printf("obj->GameMethod.direction._0	%d\n", obj->GameMethod.direction._0);
		printf("obj->GameMethod.direction._3	%d\n", obj->GameMethod.direction._3);
		printf("obj->GameMethod.direction._6	%d\n", obj->GameMethod.direction._6);
		printf("obj->GameMethod.direction._9	%d\n", obj->GameMethod.direction._9);
		printf("obj->GameMethod.direction._12	%d\n", obj->GameMethod.direction._12);
		printf("obj->GameMethod.direction._15	%d\n", obj->GameMethod.direction._15);
		printf("obj->GameMethod.direction._18	%d\n", obj->GameMethod.direction._18);
		printf("obj->GameMethod.direction._21	%d\n", obj->GameMethod.direction._21);
	}*/
	obj->GameMethod.direction = false;//向き方向リセット
	//obj->GameMethod.prevLocation = obj->GameMethod.location;
}

// エリア範囲チェック
void Actor::areaCheck(OBJ3D * obj)
{
	/*if(WinFunc::GetScreenHeight(obj->) > obj->position.y)
	obj->position.y;*/
}



// 画面1弾範囲
void Actor::ShotAreaCheck(OBJ3D * obj)
{
	int x = WinFunc::GetScreenWidth(obj->windowNum);
	int y = WinFunc::GetScreenHeight(obj->windowNum);
	x = 300;
	y = 300;
	//範囲外に出れば消去(消去時,別画面での弾生成のフラグを渡す)
	if (x < obj->position.x ||
		(x*-1) > obj->position.x)
	{
		obj->eraseAlg = &eraceShoot;
	}
	if (y < obj->position.y ||
		y*-1 > obj->position.y)
	{
		obj->eraseAlg = &eraceShoot;
	}
}



// 画面２弾範囲
void Actor::ShotAreaCheck2(OBJ3D * obj)
{
	int x = WinFunc::GetScreenWidth(obj->windowNum);
	int y = WinFunc::GetScreenHeight(obj->windowNum);
	x = 600;
	y = 0;
	if (x < obj->position.x ||
		(x*-1) > obj->position.x)
	{
		obj->eraseAlg = &eraceShotFallEnd;
	}
	if (/*y < obj->position.y ||*/
		y*-1 > obj->position.y)
	{
		obj->eraseAlg = &eraceShotFallEnd;
	}
}










/////////////////////////////////////////////////
//+ 継承メソッド
//////////////////////////////////////////////


//////////////////////////////////////////////
// コマンド(ctrl==trueの状態なら、コマンド取得)
//天球上での位置による移動方向分岐
bool inComandShiftSwitch(OBJ3D * obj)
{
//#define RECT5 108//5角形１つの角度
	bool moveFlag = false;

	switch (obj->GameMethod.location)
	{
		case ENUM::STATE_POS::STATE_CENTER:// キャラクターの位置が星の中央
			obj->GameMethod.location = ENUM::STATE_CENTER;
			moveFlag = true;
			//上キーが押されていたら
			if (obj->Command.key._0)
			{
				obj->GameMethod.direction._0 = true;//上方向振り向き
				AngleRelateStar(obj);//角度代入
				obj->GameMethod.location = ENUM::STATE_TOP;//星の頂点に移動
			}
			//右上キーが押されていたら//右キーが押されていたら
			if (obj->Command.key._3 || obj->Command.key._6)
			{
				obj->GameMethod.direction._3 = true;//右上方向振り向き
				AngleRelateStar(obj);
				obj->GameMethod.location = ENUM::STATE_RIGHT;//星の右に移動
			}
			//右下キーが押されていたら
			if (obj->Command.key._9)
			{
				obj->GameMethod.direction._9 = true;//右下方向振り向き
				AngleRelateStar(obj);
				obj->GameMethod.location = ENUM::STATE_RIGHT_DOWN;//星の右下に移動
			}
			//下には移動しない。
			if (obj->Command.key._12)
			{
				moveFlag = false;
				//obj->GameMethod.direction._12 = true;//下方向振り向き
			}
			//左下キーが押されていたら
			if (obj->Command.key._15)
			{
				obj->GameMethod.direction._15 = true;//左下方向振り向き
				AngleRelateStar(obj);
				obj->GameMethod.location = ENUM::STATE_LEFT_DOWN;//星の左下に移動
			}
			//左キーが押されていたら//左上キーが押されていたら
			if (obj->Command.key._18 || obj->Command.key._21)
			{
				obj->GameMethod.direction._21 = true;//左上方向振り向き
				AngleRelateStar(obj);
				obj->GameMethod.location = ENUM::STATE_LEFT;//星の左に移動
			}
			break;

		case ENUM::STATE_TOP:// 星の頂上では
			obj->GameMethod.location = ENUM::STATE_TOP;
			if (obj->Command.key._15)
			{
				moveFlag = true;
				obj->GameMethod.direction._15 = true;
				AngleRelateStar(obj);
				obj->GameMethod.location = ENUM::STATE_LEFT;//星の左頂点に移動
			}
			if (obj->Command.key._9)
			{
				moveFlag = true;
				obj->GameMethod.direction._9 = true;
				AngleRelateStar(obj);
				obj->GameMethod.location = ENUM::STATE_RIGHT;//星の右頂点に移動
			}
			if (obj->Command.key._12)
			{
				moveFlag = true;
				obj->GameMethod.direction._12 = true;
				AngleRelateStar(obj);
				obj->GameMethod.location = ENUM::STATE_CENTER;//星の中央に移動
			}
			break;

		case ENUM::STATE_LEFT:// 星の左では
			obj->GameMethod.location = ENUM::STATE_LEFT;
			if (obj->Command.key._3 || obj->Command.key._0)
			{
				moveFlag = true;
				obj->GameMethod.direction._3 = true;
				AngleRelateStar(obj);
				obj->GameMethod.location = ENUM::STATE_TOP;//星の頂上に移動
			}
			if (obj->Command.key._6)
			{
				moveFlag = true;
				obj->GameMethod.direction._6 = true;
				AngleRelateStar(obj);
				obj->GameMethod.location = ENUM::STATE_CENTER;//星の中央に移動
			}
			if (obj->Command.key._9 || obj->Command.key._12)
			{
				moveFlag = true;
				obj->GameMethod.direction._9 = true;
				AngleRelateStar(obj);
				obj->GameMethod.location = ENUM::STATE_LEFT_DOWN;//星の左下に移動
			}
			break;

		case ENUM::STATE_RIGHT:// 星の右では
			obj->GameMethod.location = ENUM::STATE_RIGHT;
			if (obj->Command.key._21 || obj->Command.key._0)
			{
				moveFlag = true;
				obj->GameMethod.direction._21 = true;
				AngleRelateStar(obj);
				obj->GameMethod.location = ENUM::STATE_TOP;//星の頂上に移動
			}
			if (obj->Command.key._18)
			{
				moveFlag = true;
				obj->GameMethod.direction._18 = true;
				AngleRelateStar(obj);
				obj->GameMethod.location = ENUM::STATE_CENTER;//星の中央に移動
			}
			if (obj->Command.key._15 || obj->Command.key._12)
			{
				moveFlag = true;
				obj->GameMethod.direction._15 = true;
				AngleRelateStar(obj);
				obj->GameMethod.location = ENUM::STATE_RIGHT_DOWN;//星の右下に移動
			}
			break;

		case ENUM::STATE_LEFT_DOWN:// 星の左下では
			obj->GameMethod.location = ENUM::STATE_LEFT_DOWN;
			if (obj->Command.key._3)
			{
				moveFlag = true;
				obj->GameMethod.direction._3 = true;
				AngleRelateStar(obj);
				obj->GameMethod.location = ENUM::STATE_CENTER;//星の中央に移動
			}
			if (obj->Command.key._0 || obj->Command.key._21)
			{
				moveFlag = true;
				obj->GameMethod.direction._21 = true;
				AngleRelateStar(obj);
				obj->GameMethod.location = ENUM::STATE_LEFT;//星の左に移動
			}
			if (obj->Command.key._6)
			{
				moveFlag = true;
				obj->GameMethod.direction._6 = true;
				AngleRelateStar(obj);
				obj->GameMethod.location = ENUM::STATE_RIGHT_DOWN;//星の右下に移動
			}
			break;

		case ENUM::STATE_RIGHT_DOWN:// 星の右下では
			obj->GameMethod.location = ENUM::STATE_RIGHT_DOWN;
			if (obj->Command.key._21)
			{
				moveFlag = true;
				obj->GameMethod.direction._21 = true;
				AngleRelateStar(obj);
				obj->GameMethod.location = ENUM::STATE_CENTER;//星の中央に移動
			}
			if (obj->Command.key._0 || obj->Command.key._3)
			{
				moveFlag = true;
				obj->GameMethod.direction._3 = true;
				AngleRelateStar(obj);
				obj->GameMethod.location = ENUM::STATE_RIGHT;//星の右に移動
			}
			if (obj->Command.key._18)
			{
				moveFlag = true;
				obj->GameMethod.direction._18 = true;
				AngleRelateStar(obj);
				obj->GameMethod.location = ENUM::STATE_LEFT_DOWN;//星の左下に移動
			}
			break;
		default:
			break;
	}

	//AngleRelateStar(obj);//振り向き角度
	return moveFlag;
}





//shiftコマンド
void inComandShift(OBJ3D * obj)
{
	if (obj->Command.shift != obj->PrevCommand.shift)//キー入力時1回だけ、呼ばれるように
	{
		char* kaomojiJump = nullptr;
		char* kaomojiJump2[] = { "(＞＜)" ,"(〜o〜)",nullptr };//nullはプレイヤーなし
		kaomojiJump = kaomojiJump2[obj->playerNum];
		printf("ぴょ〜ん!\n");
		printf("%s   \n", kaomojiJump);
	}
}


//移動コマンド
void inComandMove(OBJ3D * obj)
{
	if (obj->Command.key != obj->PrevCommand.key)//キー入力時1回だけ、呼ばれるように
	{
		char* kaomoji2[] = { "(・_・)", "(？_？)", nullptr };
		char* kaomoji = kaomoji2[obj->playerNum];
		printf("こっち!");
		printf("%s   \n", kaomoji);
	}

}


// 攻撃スイッチ
void atkSwich(OBJ3D * obj)
{
	switch (obj->GameMethod.location)
	{
		case ENUM::STATE_POS::STATE_CENTER:// キャラクターの位置が星の中央
			break;
		case ENUM::STATE_TOP:// 星頂上
			if (obj->Command.a && !obj->Command.b && !obj->Command.c)//反時計回り
			{
				obj->ShotInfo.statePos = ENUM::SHOT_POS::SHOT_LEFT;
			}
			if (!obj->Command.a && obj->Command.b && !obj->Command.c)//同じロケーション
			{
				obj->ShotInfo.statePos = ENUM::SHOT_POS::SHOT_TOP;
			}
			if (!obj->Command.a && !obj->Command.b && obj->Command.c)//時計回り
			{
				obj->ShotInfo.statePos = ENUM::SHOT_POS::SHOT_RIGHT;
			}
			break;
		case ENUM::STATE_LEFT:// 星の左では
			if (obj->Command.a && !obj->Command.b && !obj->Command.c)//反時計回り
			{
				obj->ShotInfo.statePos = ENUM::SHOT_POS::SHOT_LEFT_DOWN;
			}
			if (!obj->Command.a && obj->Command.b && !obj->Command.c)//同じロケーション
			{
				obj->ShotInfo.statePos = ENUM::SHOT_POS::SHOT_LEFT;
			}
			if (!obj->Command.a && !obj->Command.b && obj->Command.c)//時計回り
			{
				obj->ShotInfo.statePos = ENUM::SHOT_POS::SHOT_TOP;
			}
			break;
		case ENUM::STATE_RIGHT:
			if (obj->Command.a && !obj->Command.b && !obj->Command.c)//反時計回り
			{
				obj->ShotInfo.statePos = ENUM::SHOT_POS::SHOT_TOP;
			}
			if (!obj->Command.a && obj->Command.b && !obj->Command.c)//同じロケーション
			{
				obj->ShotInfo.statePos = ENUM::SHOT_POS::SHOT_RIGHT;
			}
			if (!obj->Command.a && !obj->Command.b && obj->Command.c)//時計回り
			{
				obj->ShotInfo.statePos = ENUM::SHOT_POS::SHOT_RIGHT_DOWN;
			}
			break;
		case ENUM::STATE_LEFT_DOWN:
			if (obj->Command.a && !obj->Command.b && !obj->Command.c)//反時計回り
			{
				obj->ShotInfo.statePos = ENUM::SHOT_POS::SHOT_RIGHT_DOWN;
			}
			if (!obj->Command.a && obj->Command.b && !obj->Command.c)//同じロケーション
			{
				obj->ShotInfo.statePos = ENUM::SHOT_POS::SHOT_LEFT_DOWN;
			}
			if (!obj->Command.a && !obj->Command.b && obj->Command.c)//時計回り
			{
				obj->ShotInfo.statePos = ENUM::SHOT_POS::SHOT_LEFT;
			}
			break;
		case ENUM::STATE_RIGHT_DOWN:
			if (obj->Command.a && !obj->Command.b && !obj->Command.c)//反時計回り
			{
				obj->ShotInfo.statePos = ENUM::SHOT_POS::SHOT_RIGHT;
			}
			if (!obj->Command.a && obj->Command.b && !obj->Command.c)//同じロケーション
			{
				obj->ShotInfo.statePos = ENUM::SHOT_POS::SHOT_RIGHT_DOWN;
			}
			if (!obj->Command.a && !obj->Command.b && obj->Command.c)//時計回り
			{
				obj->ShotInfo.statePos = ENUM::SHOT_POS::SHOT_LEFT_DOWN;
			}
			break;
		default:
			break;
	}
}


//攻撃コマンド
void inComandAtk(OBJ3D * obj)
{
	if (obj->Command.a != obj->PrevCommand.a ||
		obj->Command.b != obj->PrevCommand.b ||
		obj->Command.c != obj->PrevCommand.c
		)//キー入力時1回だけ、呼ばれるように
	{
		char* kaomojiAtk2[] = { "(_ _ )", "(＾0＾)", nullptr };
		char* kaomojiAtk = kaomojiAtk2[obj->playerNum];
		printf("えい!\n");
		printf("%s   \n", kaomojiAtk);
	}
}







////////////////////////
// AI
//AI easy
void Actor::AI_EASY(OBJ3D * obj)
{
	//キー入力可能でない
	if (!obj->CharInfo.ctrl)
		return;

	if (pStageManager->GetTurn(ENUM::PLAYER_2) == ENUM::ENEMY_TURN)
	{

		if ((obj->CharInfo.AItimer % SEC * 2) == 0 && obj->CharInfo.AItimer != 0)
		{
			// ランダム方向を向く
			switch ((rand() % ENUM::STATE_RIGHT_DOWN + 1))
			{
				case ENUM::STATE_CENTER:
					if (rand() % 2 == 0)
					{
						obj->Command.key._21 = true;
					}
					else
					{
						obj->Command.key._3 = true;
					}
					break;
				case ENUM::STATE_TOP:
					obj->Command.key._0 = true;
					break;
				case ENUM::STATE_LEFT:
					obj->Command.key._6 = true;
					break;
				case ENUM::STATE_RIGHT:
					obj->Command.key._18 = true;
					break;
				case ENUM::STATE_LEFT_DOWN:
					obj->Command.key._15 = true;
					break;
				case ENUM::STATE_RIGHT_DOWN:
					obj->Command.key._9 = true;
					break;
				default:
					break;
			}
			if (!obj->Command.shift)
			{
				inComandMove(obj);//コンソール文字表示
			}
			//ワープまでの時間
			if (obj->CharInfo.AItimer > SEC * 10)
			{
				if (inComandShiftSwitch(obj))//天球上の位置によるコマンド分岐
				{
					obj->Command.shift = true;
					inComandShift(obj);//コンソール文字表示
					obj->CharInfo.AItimer = 0;
				}
			}
		}



		//攻撃ボタン分岐
		auto comFunc = [](OBJ3D * obj) {
			if (!obj->Command.a && !obj->Command.b && !obj->Command.c)return;

			//攻撃ボタン分岐
			atkSwich(obj);

			if (obj->GameMethod.location == ENUM::STATE_CENTER) return;
			MoveAlgHelper* moveAlgHelper[1] = { nullptr };

			//コンソール文字表示
			inComandAtk(obj);
		};

		//真ん中以外にいたら攻撃
		if (obj->GameMethod.location != ENUM::STATE_CENTER)
		{
			if (obj->CharInfo.AItimer % ((rand() % SEC) + SEC) == 0 && obj->CharInfo.AItimer != 0)
			{
				obj->Command.a = true;
			}
			comFunc(obj);
		}
	}
	else
	{

		if ((obj->CharInfo.AItimer % SEC ) == 0 && obj->CharInfo.AItimer != 0)
		{
			// ランダム方向を向く
			switch ((rand() % ENUM::STATE_RIGHT_DOWN + 1))
			{
				case ENUM::STATE_CENTER:
					if (rand() % 2 == 0)
					{
						obj->Command.key._21 = true;
					}
					else
					{
						obj->Command.key._3 = true;
					}
					break;
				case ENUM::STATE_TOP:
					obj->Command.key._0 = true;
					break;
				case ENUM::STATE_LEFT:
					obj->Command.key._6 = true;
					break;
				case ENUM::STATE_RIGHT:
					obj->Command.key._18 = true;
					break;
				case ENUM::STATE_LEFT_DOWN:
					obj->Command.key._15 = true;
					break;
				case ENUM::STATE_RIGHT_DOWN:
					obj->Command.key._9 = true;
					break;
				default:
					break;
			}
			if (!obj->Command.shift)
			{
				inComandMove(obj);//コンソール文字表示
			}
			//ワープまでの時間
			if (obj->CharInfo.AItimer > SEC * 3)
			{
				if (inComandShiftSwitch(obj))//天球上の位置によるコマンド分岐
				{
					obj->Command.shift = true;
					inComandShift(obj);//コンソール文字表示
					obj->CharInfo.AItimer = 0;
				}
			}
		}



		//攻撃ボタン分岐
		auto comFunc = [](OBJ3D * obj) {
			if (!obj->Command.a && !obj->Command.b && !obj->Command.c)return;

			//攻撃ボタン分岐
			atkSwich(obj);

			if (obj->GameMethod.location == ENUM::STATE_CENTER) return;
			MoveAlgHelper* moveAlgHelper[1] = { nullptr };

			//コンソール文字表示
			inComandAtk(obj);
		};

		//真ん中以外にいたら攻撃
		if (obj->GameMethod.location != ENUM::STATE_CENTER)
		{
			if (obj->CharInfo.AItimer % ((rand() % SEC) + SEC) == 0 && obj->CharInfo.AItimer != 0)
			{
				obj->Command.a = true;
			}
			comFunc(obj);
		}
	}

	obj->CharInfo.AItimer++;
}



//AI normal
void Actor::AI_NORMAL(OBJ3D * obj)
{
	//キー入力可能でない
	if (!obj->CharInfo.ctrl)
		return;
	//エネミーターンなら
	if (pStageManager->GetTurn(ENUM::PLAYER_2) == ENUM::ENEMY_TURN)
	{
		//ワープまでの時間
		if (obj->CharInfo.AItimer > SEC * 3)
		{
			obj->Command.shift = true;
		}
	}
}



//AI hard
void Actor::AI_HARD(OBJ3D * obj)
{
	//キー入力可能でない
	if (!obj->CharInfo.ctrl)
		return;

	//WaitState
	if (obj->actorState == pWaitState)
	{
		//ワープまでの時間
		if (obj->CharInfo.AItimer > SEC)
		{
			obj->Command.shift = true;
		}
	}
	if (obj->actorState == pWalkState)
	{
		obj->Command.shift = true;
	}
	obj->CharInfo.AItimer++;
}







////////////////////////////
// コマンド分岐
void Actor::CommandSwitch(OBJ3D * obj)
{

	// 向き
	{
		//L1を押しながら移動キーで移動
		auto comFunc = [](OBJ3D * obj) {
			if (input::STATE(0) & input::PAD_L1)//L1押したら
			{
				if (inComandShiftSwitch(obj))//天球上の位置によるコマンド分岐
				{
					obj->Command.shift = true;
					inComandShift(obj);//コンソール文字表示
				}
			}
			if (!obj->Command.shift)
			{
				inComandMove(obj);//コンソール文字表示
			}
		};

		//左
		if ((input::STATE(0) & input::PAD_LEFT) && !(input::STATE(0) & input::PAD_UP) && !(input::STATE(0) & input::PAD_DOWN))
		{
			obj->Command.key._18 = true;
			comFunc(obj);//ローカル関数
		}
		//右
		if ((input::STATE(0) & input::PAD_RIGHT) && !(input::STATE(0) & input::PAD_UP) && !(input::STATE(0) & input::PAD_DOWN))
		{
			obj->Command.key._6 = true;
			comFunc(obj);
		}

		//上
		if (input::STATE(0) & input::PAD_UP && !(input::STATE(0) & input::PAD_LEFT) && !(input::STATE(0) & input::PAD_RIGHT))
		{
			obj->Command.key._0 = true;
			comFunc(obj);
		}
		//左上
		if ((input::STATE(0) & input::PAD_UP) && (input::STATE(0) & input::PAD_LEFT))
		{
			obj->Command.key = false;
			obj->Command.key._21 = true;
			comFunc(obj);
		}
		//右上
		if ((input::STATE(0) & input::PAD_UP) && (input::STATE(0) & input::PAD_RIGHT))
		{
			obj->Command.key = false;
			obj->Command.key._3 = true;
			comFunc(obj);
		}

		//下
		if (input::STATE(0) & input::PAD_DOWN && !(input::STATE(0) & input::PAD_LEFT) && !(input::STATE(0) & input::PAD_RIGHT))
		{
			obj->Command.key._12 = true;
			comFunc(obj);
		}
		//左下
		if ((input::STATE(0) & input::PAD_DOWN) && (input::STATE(0) & input::PAD_LEFT))
		{
			obj->Command.key = false;
			obj->Command.key._15 = true;
			comFunc(obj);
		}
		//右下
		if ((input::STATE(0) & input::PAD_DOWN) && (input::STATE(0) & input::PAD_RIGHT))
		{
			obj->Command.key = false;
			obj->Command.key._9 = true;
			comFunc(obj);
		}

	}


	// 攻撃
	{
		//攻撃ボタン分岐
		auto comFunc = [](OBJ3D * obj) {
			//攻撃ボタン分岐
			atkSwich(obj);

			if (obj->GameMethod.location == ENUM::STATE_CENTER) return;

			//コンソール文字表示
			inComandAtk(obj);
		};

		//１つ逆時計周りの方向へ
		if ((input::TRG(0) & input::PAD_TRG1) && !(input::TRG(0) &  input::PAD_TRG2) && !(input::TRG(0) & input::PAD_TRG3))
		{
			obj->Command.a = true;
			comFunc(obj);//攻撃ボタン分岐
		}
		//同じ所へ
		if ((input::TRG(0) & input::PAD_TRG2) && !(input::TRG(0) &  input::PAD_TRG1) && !(input::TRG(0) & input::PAD_TRG3))
		{
			obj->Command.b = true;
			comFunc(obj);//攻撃ボタン分岐
		}
		//１つ時計周りの方向へ
		if ((input::TRG(0) & input::PAD_TRG3) && !(input::TRG(0) &  input::PAD_TRG1) && !(input::TRG(0) & input::PAD_TRG2))
		{
			obj->Command.c = true;
			comFunc(obj);//攻撃ボタン分岐
		}


		//if (GetAsyncKeyState(VK_SPACE) < 0 && (obj->timer % SEC == 0))
		//{
		//	obj->Command.a = true;
		//	comFunc(obj);//攻撃ボタン分岐
		//}
	}

}




/////////////////////////////////
// ステート遷移
void Actor::StateDefChange(OBJ3D * obj)
{
	// 待機ステート
	if (obj->actorState == pWaitState)
	{
		//シフトコマンド取得時、移動ステートへ
		if (obj->Command.shift)
		{
			obj->actorState = pWalkState;
			obj->stateMethodWait.timer = 0;
			obj->stateMethodWait.state = 0;
		}
		// 攻撃条件
		//発射コマンド取得時、攻撃ステートへ
		if (obj->Command.a || obj->Command.b || obj->Command.c)
		{
			obj->actorState = pAttackState;
			obj->stateMethodWait.timer = 0;
			obj->stateMethodWait.state = 0;
		}
	}


	// 移動ステート
	if (obj->actorState == pWalkState)//コマンド受付はしないステート
	{
		//一定時間で待機ステートへ
		if (obj->stateMethodWalk.timer > 34 )
		{
			//if (obj->speed.x == 0 && obj->speed.z == 0)
			{
				obj->actorState = pWaitState;
				obj->stateMethodWalk.timer = 0;
				obj->stateMethodWalk.state = 0;
			}
			//位置修正
			//if(obj->GameMethod.location == ENUM::STATE_CENTER || obj->GameMethod.location == ENUM::STATE_TOP)
			obj->position = Location[obj->GameMethod.location];
		}

	}

	// 攻撃ステート
	if (obj->actorState == pAttackState)//コマンド受付はしないステート
	{
		//アニメーションの終わりに待機ステートへ
		// バリアーか玉発射か
		//switch (obj->playerNum)
		//{
		//	case ENUM::PLAYER_1:
		//		if (pStageManager->GetTurn(obj->playerNum) == ENUM::PLAYER_TURN)
		//		{
		//			//星
		//			if (obj->stateMethodAttack.isShoot)
		//			{
		//				obj->actorState = pWaitState;
		//				obj->stateMethodAttack.timer = 0;
		//				obj->stateMethodAttack.state = 0;
		//			}
		//		}
		//		else
		//		{
		//			//バリア消す
		//			if (obj->Model.GetFlagBoneAnimationEnd())
		//			{
		//				if (obj->playerNum == ENUM::PLAYER_1)
		//				{
		//					pBalyerManager->Uninit();
		//				}
		//				else
		//				{
		//					pBalyerManager2->Uninit();
		//				}
		//				obj->actorState = pWaitState;
		//				obj->stateMethodAttack.timer = 0;
		//				obj->stateMethodAttack.state = 0;
		//			}
		//		}
		//		break;
		//	case ENUM::PLAYER_2:
		//		if (pStageManager->GetTurn(obj->playerNum) == ENUM::PLAYER_TURN)
		//		{
		//			//バリア消す
		//			if (obj->Model.GetFlagBoneAnimationEnd())
		//			{
		//				if (obj->playerNum == ENUM::PLAYER_1)
		//				{
		//					pBalyerManager->Uninit();
		//				}
		//				else
		//				{
		//					pBalyerManager2->Uninit();
		//				}
		//				obj->actorState = pWaitState;
		//				obj->stateMethodAttack.timer = 0;
		//				obj->stateMethodAttack.state = 0;
		//			}
		//		}
		//		else
		//		{
		//			//星
		//			if (obj->stateMethodAttack.isShoot)
		//			{
		//				obj->actorState = pWaitState;
		//				obj->stateMethodAttack.timer = 0;
		//				obj->stateMethodAttack.state = 0;
		//			}
		//		}
		//		break;
		//	default:
		//		break;
		//}
		//バリア消す
		if (obj->Model.GetFlagBoneAnimationEnd())
		{
			if (obj->playerNum == ENUM::PLAYER_1)
			{
				pBalyerManager->Uninit();
			}
			else
			{
				pBalyerManager2->Uninit();
			}
			obj->actorState = pWaitState;
			obj->stateMethodAttack.timer = 0;
			obj->stateMethodAttack.state = 0;
		}

	}

}


//関数を変える
void Actor::FuncChanger(OBJ3D * obj)
{
	// バリアーか玉発射か
	switch (obj->playerNum)
	{
		case ENUM::PLAYER_1:
			if (pStageManager->GetTurn(obj->playerNum) == ENUM::PLAYER_TURN)
			{
				obj->Behavier.attackState = AttckDefault;
			}
			else
			{
				obj->Behavier.attackState = AttckBalyer;
			}
			break;
		case ENUM::PLAYER_2:
			if (pStageManager->GetTurn(obj->playerNum) == ENUM::PLAYER_TURN)
			{
				obj->Behavier.attackState = AttckBalyer;
			}
			else
			{
				obj->Behavier.attackState = AttckDefault;
			}
			break;
		default:
			break;
	}

}





// モデルに反映
void Actor::Relate(OBJ3D * obj)
{
	//反映
	obj->Model.scale = obj->scale;
	obj->Model.angle = obj->angle;
	obj->Model.pos = obj->position;
	obj->Model.color = obj->color;
}
