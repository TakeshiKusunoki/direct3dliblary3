#pragma once
#include <directxmath.h>
#include "3DLib\\Skinned_mesh.h"


class MyMesh
{
private:
	bool					bLoad;



public:
	Skinned_mesh* obj;//	メッシュ情報
	DirectX::XMFLOAT3	pos;//	座標
	DirectX::XMFLOAT3	angle;//	回転角度
	DirectX::XMFLOAT3	scale;//	大きさ
	DirectX::XMFLOAT4	color;//	メッシュの色


	//	情報の初期化
	void	Initialize();

	//	FBXの読込
	bool	Load(const char* fbx_filename);

	//	プリミティブの設定
	//	引数
	//primitive:メッシュモデル
	void	SetPrimitive(Skinned_mesh* primitive);

	//	既存MyMeshデータの使い回し
	//	引数
	//primitive:メッシュモデル
	void	SetMesh(MyMesh& org);

	//	メッシュの解放
	void	Release();

	//	引数
	//	view:ビュー変換行列
	//	projection:投影変換行列
	//  elapsed_time : タイマー
	void Render(const DirectX::XMMATRIX & view, const DirectX::XMMATRIX & projection, float elapsed_time);

	///bool StanbyPresant(const DirectX::XMMATRIX & view, const DirectX::XMMATRIX & projection, float elapsed_time);


	//	ワールド変換行列の取得
	DirectX::XMMATRIX	GetWorldMatrix();

	// アニメーションの終わり
	bool GetFlagBoneAnimationEnd()
	{
		return obj->GetFlagBoneAnimationEnd();
	}
};

