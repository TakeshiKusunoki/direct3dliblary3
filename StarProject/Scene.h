#pragma once
#include "BaseLib\\winMainFunc.h"
#include "BaseLib\\high_resolution_timer.h"
#include "3DLib\sprite2D.h"
#include "3DLib\\Font.h"
#include "3DLib\Wrapped.h"
// Sceneインターフェイス
class IScene
{
public:
	virtual IScene* execute() = 0;
protected:
	static high_resolution_timer* Timer[WINDOW_NUM];
};




// Sceneクラス
class Scene : public IScene
{
private:
	IScene*  pNext;    // 次のシーン
	static bool standByMode;//スタンバイ状態を管理するstatic変数
	ID3D11RenderTargetView* p_RenderTargetView[WINDOW_NUM];//フレームワークの数まで
	ID3D11DepthStencilView* p_DepthStencilView[WINDOW_NUM];//フレームワークの数まで


	LONGLONG CountsPerSecond;
	LONGLONG CountsPerFrame;//1/60秒
	LONGLONG NextCounts;
protected:
	//timer
	Sprite2D* spr[100];
public:
	IScene* execute();

	virtual void init(UINT i = 0) {
		for (int i = 0; i < 100; i++)
		{
			spr[i] = nullptr;
		}

	}
	virtual void uninit(UINT i_ = 0) {
		if (i_ == Enum::WINDOW_2)
		{
			for (int i = 0; i < 100; i++)
			{
				if (spr[i])delete[] spr[i];
				spr[i] = nullptr;
			}
		}
	}
	virtual void update(UINT i = 0) {}
	virtual void draw(UINT i = 0) {}

	void setScene(IScene* scene)
	{
		pNext = scene;
	}
private:
	bool StanbyPresant(UINT i = 0);
	void Calculate_frame_stats(UINT i = 0);
};




// Scene管理クラス
class SceneManager :public Singleton<SceneManager>
{
public:
	void execute(IScene* scene);
private:

};
// インスタンス取得マクロ
#define pSceneManager (SceneManager::getInstance())




//+ ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//+ それぞれのシーン


// シーン初期化
class SceneInit :public IScene, public Singleton<SceneInit>
{
public:
	IScene* execute();
};
#define pSceneInit (SceneInit::getInstance())






// タイトルシーン
class SceneTitle : public Scene, public Singleton<SceneTitle>
{
private:
	int		timer;
public:
	void init(UINT i = 0);
	void update(UINT i = 0);
	void draw(UINT i = 0);
};
// インスタンス取得マクロ
#define pSceneTitle (SceneTitle::getInstance())






// ゲームシーン
class SceneGame : public Scene, public Singleton<SceneGame>
{
private:
	int		state;
	int		timer;
public:
	void init(UINT i = 0);
	void update(UINT i = 0);
	void draw(UINT i = 0);
	void uninit(UINT i);
	float GetTimer(UINT i = 0)
	{
		return Timer[i]->time_interval();
	}
};
// インスタンス取得マクロ
#define pSceneGame (SceneGame::getInstance())

//
//
//
//
//// ゲームオーバーシーン
//class SceneOver : public Scene, public Singleton<SceneOver>
//{
//private:
//	int		timer;
//public:
//	void init();
//	void update();
//	void draw();
//};
//
//// インスタンス取得マクロ
//#define pSceneOver (SceneOver::getInstance())
//
//
//
//
//
//
//// クリアシーン
//class SceneClear : public Scene, public Singleton<SceneClear>
//{
//private:
//	int		timer;
//public:
//	void init();
//	void update();
//	void draw();
//};
//// インスタンス取得マクロ
//#define pSceneClear (SceneClear::getInstance())
//
//
//
//
//
//// デモシーン
//class SceneDemo : public Scene, public Singleton<SceneDemo>
//{
//private:
//	int timer;//タイマー
//public:
//	void init();
//	void update();
//	void draw();
//};
////インスタンス作成
//#define pSceneDemo (SceneDemo::getInstance())
//
//
//
//
