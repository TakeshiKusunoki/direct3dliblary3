#pragma once
#include "3DLib\sprite2D.h"
#define PLAYERNUM 2
// StageManagerクラス
class BaseStageManager
{
public:
	static UINT WINDOW_NUMBER;
protected:
	int				state;
	int				timer;

	static UINT turn[PLAYERNUM];//どちらのターンか
	static UINT turnNum;//ターン番号(ターンが変わったら増える)

	DirectX::XMMATRIX view;//ビュー行列
	DirectX::XMMATRIX projection;//プロジェクション行列
	DirectX::XMFLOAT4 light_direction;//光の方向
	DirectX::XMFLOAT4 lightColor;			//光の色
	DirectX::XMFLOAT4 nyutoralLightColor;	//環境光
	float elapsed_time;/*UNIT.23*/
	Sprite2D* spr[100];

	static bool shotAdd;//ウインドウ２に弾追加
	static bool shot2Add;//ウインドウ２に弾追加
public:
	static UINT point[PLAYERNUM];//獲得ポイント
	static bool endFlag;//終了フラグ
public:
	virtual void init() {};
	virtual void update() {};
	virtual void draw() {};

	DirectX::XMFLOAT4 Getlight_direction()
	{
		return light_direction;
	}
	DirectX::XMFLOAT4 GetlightColor()
	{
		return lightColor;
	}
	DirectX::XMFLOAT4 GetnyutoralLightColor()
	{
		return nyutoralLightColor;
	}
	static UINT GetWindowNumber()
	{
		return WINDOW_NUMBER;
	}
	//セッター
	static void SetShot2Add(bool f)
	{
		shot2Add = f;
	}
	//ゲッター
	static UINT GetTurn(UINT i = 0)
	{
		return turn[i];
	}

};


//ウインドウ１
class StageManager : public BaseStageManager, public Singleton<StageManager>
{
public:
	void init();
	void update();
	void draw();
	void uninit();

};
// インスタンス取得
#define	pStageManager		(StageManager::getInstance())

//ウインドウ2
class StageManager2 : public BaseStageManager,public Singleton<StageManager2>
{
public:
	void init();
	void update();
	void draw();
	void uninit();
};
#define	pStageManager2		(StageManager2::getInstance())


//ウインドウ3
class StageManager3 : public BaseStageManager, public Singleton<StageManager3>
{
public:
	void init();
	void update();
	void draw();
	void uninit();
};
#define	pStageManager3		(StageManager3::getInstance())