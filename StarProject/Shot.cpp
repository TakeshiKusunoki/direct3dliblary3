#include "All.h"
EXTERN MyMesh* Models;
DirectX::XMFLOAT3 Location_[] = {
	//{ 0,0,0 },//中央
	{ 0,0,-100 },//頂上
	{ 100,0,-40 },//左
	{ -100,0,-40 },//右
	{ 70,0,80 },//左下
	{ -70,0,80 },//右下
};


//******************************************************************************
//
//      ShotShoot
//
//******************************************************************************
void ShotShoot::move(OBJ3D * obj)
{
	switch (obj->state)
	{
		case 0:
			switch (obj->playerNum)
			{
				case ENUM::PLAYER_1:
					obj->Model = Models[ENUM::MODEL_DATANUM::STAR];
					break;
				case ENUM::PLAYER_2:
					obj->Model = Models[ENUM::MODEL_DATANUM::STAR_RED];
					break;
				default:
					obj->Model = Models[ENUM::MODEL_DATANUM::CUBE];
					break;
			}
			obj->Axcel._ = { KASOKU,KASOKU,KASOKU };
			obj->Axcel.speedMax = { SPEED_MAX ,SPEED_MAX ,SPEED_MAX };

			obj->timer = 0;
			obj->state++;
		case 1:
			//obj->speed.x += obj->Axcel._.x;
			obj->speed.y += obj->Axcel._.y/6;
			//obj->speed.z += obj->Axcel._.z;

			/*obj->angle.y = ToRadian(obj->angle.y);
			obj->angle.z = ToRadian(1.0f);*/



			ShotShootSppedRelate(obj);
			ShotAreaCheck(obj);
			if (obj->timer % SEC*SEC == 0)
			{
				printf("obj->position.x%f	", obj->position.x);
				printf("obj->position.y%f	", obj->position.y);
				printf("obj->position.z%f	\n", obj->position.z);
				printf("%f	\n", obj->angle.z);
			}

			Relate(obj);
			obj->timer++;
			break;
		default:
			break;
	}
}




//******************************************************************************
//
//      ShotFallTop
//
//******************************************************************************
void ShotFall::move(OBJ3D * obj)
{
	float y = 140;
	switch (obj->state)
	{
		case 0:
			switch (obj->playerNum)
			{
				case ENUM::PLAYER_1:
					obj->Model = Models[ENUM::MODEL_DATANUM::STAR];
					break;
				case ENUM::PLAYER_2:
					obj->Model = Models[ENUM::MODEL_DATANUM::STAR_RED];
					break;
				default:
					obj->Model = Models[ENUM::MODEL_DATANUM::CUBE];
					break;
			}

			obj->Axcel._ = { KASOKU,KASOKU,KASOKU };
			obj->Axcel.speedMax = { SPEED_MAX ,SPEED_MAX ,SPEED_MAX };

			//落ちる位置
			switch (obj->ShotInfo.statePos)
			{
				case ENUM::SHOT_POS::SHOT_TOP:// 星頂上へ落ちる
					obj->position = Location_[obj->ShotInfo.statePos];
					break;
				case ENUM::SHOT_POS::SHOT_LEFT:// 星の左へ落ちる
					obj->position = Location_[obj->ShotInfo.statePos];
					break;
				case ENUM::SHOT_POS::SHOT_RIGHT:
					obj->position = Location_[obj->ShotInfo.statePos];
					break;
				case ENUM::SHOT_POS::SHOT_LEFT_DOWN:
					obj->position = Location_[obj->ShotInfo.statePos];
					break;
				case ENUM::SHOT_POS::SHOT_RIGHT_DOWN:
					obj->position = Location_[obj->ShotInfo.statePos];
					break;
				default:
					break;
			}
			//当たり判定用サイズ
			obj->size = { 30,30,30 };

			obj->position.y = 200;
			obj->timer = 0;
			obj->state++;
		case 1:

			obj->speed.y -= obj->Axcel._.y/10;

			//obj->angle.x += ToRadian(1);

			ShotShootSppedRelate(obj);
			ShotAreaCheck2(obj);
			Relate(obj);
			break;
		default:
			break;
	}
}











//******************************************************************************
//
//      発射弾消す
//
//******************************************************************************
void EraceShoot::erase(OBJ3D * obj)
{
	//BaseStageManager::SetShot2Add(true);
	MoveAlgHelper* moveAlgHelper[1] = { nullptr };
	switch (obj->playerNum)
	{
		case ENUM::PLAYER_1:
			pShotManager2->addShot(&shotFall, moveAlgHelper, obj->windowNum, obj->playerNum, obj->ShotInfo.statePos, DirectX::XMFLOAT3(2, 2, 2), DirectX::XMFLOAT3(0, 90, 0), obj->position);
			break;
		case ENUM::PLAYER_2:
			pShotManager->addShot(&shotFall, moveAlgHelper, obj->windowNum, obj->playerNum, obj->ShotInfo.statePos, DirectX::XMFLOAT3(2, 2, 2), DirectX::XMFLOAT3(0, 90, 0), obj->position);
			break;
		default:
			break;
	}

	if (obj->helperManager.getList())obj->helperManager.UnInit();
	obj->clear();
}

void EraceShotFallTop::erase(OBJ3D * obj)
{
	//BaseStageManager::SetShotAdd(true);
	if (obj->helperManager.getList())obj->helperManager.UnInit();
	obj->clear();
}


void EraceShotFallEnd::erase(OBJ3D * obj)
{
	pStageManager->endFlag = true;
	//防がれなかった
	if (!obj->Flag.interrapt)
	{
		pStageManager->point[obj->playerNum]++;
		printf("%dにとくてん%d点\n", obj->playerNum, pStageManager->point[obj->playerNum]);
	}
	if (obj->helperManager.getList())obj->helperManager.UnInit();
	obj->clear();
}







//******************************************************************************
//
//	ShotManager
//
//******************************************************************************
OBJ3D * ShotManager::addShot(MoveAlg * mvAlg, MoveAlgHelper * mvAlgHelper[], UINT windowNum_, int playerNum, ENUM::SHOT_POS shotStatePos, DirectX::XMFLOAT3 scale, DirectX::XMFLOAT3 angle, DirectX::XMFLOAT3 position, bool isAI, DirectX::XMFLOAT4 color)
{
	OBJ3D obj;
	obj.mvAlg = mvAlg;
	//mvAlgHelperは最後にNULLの入った配列
	for (UINT i = 0; mvAlgHelper[i] != nullptr; i++)
	{
		obj.helperManager.add(mvAlgHelper[i], position);
	}

	obj.position = position;
	obj.scale = scale;
	obj.angle.x = angle.x;
	obj.angle.y = angle.y;
	obj.angle.z = angle.z;

	obj.color = color;
	obj.windowNum = windowNum_;
	obj.playerNum = playerNum;
	obj.ShotInfo.statePos = shotStatePos;
	obj.Flag.AI = isAI;
	objList.push_back(obj);//リストに追加
	return &(*objList.rbegin());
}





OBJ3D * ShotManager2::addShot(MoveAlg * mvAlg, MoveAlgHelper * mvAlgHelper[], UINT windowNum_, int playerNum, ENUM::SHOT_POS shotStatePos, DirectX::XMFLOAT3 scale, DirectX::XMFLOAT3 angle, DirectX::XMFLOAT3 position, bool isAI, DirectX::XMFLOAT4 color)
{
	OBJ3D obj;
	obj.mvAlg = mvAlg;
	//mvAlgHelperは最後にNULLの入った配列
	for (UINT i = 0; mvAlgHelper[i] != nullptr; i++)
	{
		obj.helperManager.add(mvAlgHelper[i], position);
	}

	obj.position = position;
	obj.scale = scale;
	obj.angle.x = angle.x;
	obj.angle.y = angle.y;
	obj.angle.z = angle.z;

	obj.color = color;
	obj.windowNum = windowNum_;
	obj.playerNum = playerNum;
	obj.ShotInfo.statePos = shotStatePos;
	obj.Flag.AI = isAI;
	objList.push_back(obj);//リストに追加
	return &(*objList.rbegin());
}














//+ バリア
void Balyer::move(OBJ3D * obj)
{
	switch (obj->state)
	{
		case 0:
			switch (obj->playerNum)
			{
				case ENUM::PLAYER_1:
					obj->Model = Models[ENUM::MODEL_DATANUM::BARIYER1];
					break;
				case ENUM::PLAYER_2:
					obj->Model = Models[ENUM::MODEL_DATANUM::BARIYER2];
					break;
				default:
					obj->Model = Models[ENUM::MODEL_DATANUM::CUBE];
					break;
			}
			//当たり判定用サイズ
			obj->size = { 30,80,30 };
			obj->scale = {3,3,3};

			obj->timer = 0;
			obj->state++;
		case 1:
			obj->angle.x += ToRadian(1);
			obj->angle.z += ToRadian(1);

			Relate(obj);
			obj->timer++;
			break;
		default:
			break;
	}
}







// バリアマネージャー
void BalyerManager::Uninit()
{
	for (auto& it : objList)            // objListの全ての要素をループし、itという名前で各要素にアクセス
	{
		it.eraseAlg = &eraceObj;
	}
}

void BalyerManager2::Uninit()
{
	for (auto& it : objList)            // objListの全ての要素をループし、itという名前で各要素にアクセス
	{
		it.eraseAlg = &eraceObj;
	}
}
