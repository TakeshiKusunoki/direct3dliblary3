/////////////////////////////////////////
//
//	データファイル
//
/////////////////////////////////////////

//fbxモデルのファイルパス
const char* modelFileName[] = {
	"FBX\\hosi_aka.fbx",
	"FBX\\danbo_fbx\\danbo_taiki.fbx",
	"FBX\\danbo_fbx\\danbo_fly.fbx",
	"FBX\\danbo_fbx\\danbo_atk.fbx",
	"FBX\\danbo_fbx\\danbo_taiki.fbx",//"FBX\\taiki.fbx",
	"FBX\\danbo_fbx\\danbo_fly.fbx",//"FBX\\work.fbx",
	"FBX\\danbo_fbx\\danbo_atk.fbx",
	nullptr
};

const char* constModelFileName[] = {
	"FBX\\ucyuu.fbx",
	"FBX\\hankyu_blue.fbx",
	"FBX\\planet01.fbx",
	"FBX\\planet02.fbx",
	"FBX\\planet03.fbx",
	"FBX\\ucyuu.fbx",
	"FBX\\hosi_kiiro.fbx",
	"FBX\\hosi_aka.fbx",
	"FBX\\tama_hosi_kiiro.fbx",
	"FBX\\tama_hosi_aka.fbx",
	nullptr
};