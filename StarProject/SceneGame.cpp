//******************************************************************************
//
//
//		ゲーム処理
//
//
//******************************************************************************

#include "All.h"

enum {
	STATE_MAIN,
	STATE_OVER,
	STATE_CLEAR,
};

// ポーズクラス（ローカルクラス）
//class Pause
//{
//private:
//	int			state;			// ポーズ状態
//public:
//	void init();
//	int operator()();
//	void draw();
//};

//static Pause	pause;

//******************************************************************************
//
//		初期設定
//
//******************************************************************************

void SceneGame::init(UINT i)
{
	switch (i)
	{
		case Enum::WINDOW_0:
			pStageManager->init();			// ステージ初期設定
			break;
		case Enum::WINDOW_1:
			pStageManager2->init();
			break;
		case Enum::WINDOW_2:
			pStageManager3->init();
			break;
		default:
			break;
	}

	// メンバ変数の設定
	state = STATE_MAIN;
	//pause.init();
}

//******************************************************************************
//
//		更新処理
//
//******************************************************************************

void SceneGame::update(UINT i)
{
	// ソフトリセット
	//if ((pad_state & PAD_START) && (pad_trg & PAD_SELECT))
	//{
	//	setScene(pSceneTitle);
	//	return;
	//}

	//// ポーズ
	//if (pause()) return;

	switch (i)
	{
		case Enum::WINDOW_0:
			pStageManager->update();		// ステージ進行
			break;
		case Enum::WINDOW_1:
			pStageManager2->update();		// ステージ進行
			break;
		case Enum::WINDOW_2:
			pStageManager3->update();
			break;
		default:
			break;
	}
	if (GetAsyncKeyState('X')<0)
	{
		setScene(pSceneTitle);
	}
	// シーン切り換えチェック
	switch (state) {
	case STATE_MAIN:		// ゲーム進行中
		// ゲームオーバーチェック
		//if (!pPlayerManager->isAlive())
		/*{
			state = STATE_OVER;
			timer = 0;
		}*/
		// ゲームクリアチェック
		//if (pStageManager->isAllClear())
		/*{
			state = STATE_CLEAR;
			timer = 0;
		}*/
		break;
	//case STATE_OVER:		// ゲームオーバーへの切り換え
	//	timer++;
	//	if (timer > 60 * 2) setScene(pSceneOver);
	//	break;
	//case STATE_CLEAR:		// ゲームクリアへの切り換え
	//	timer++;
	//	if (timer > 60 * 2) setScene(pSceneClear);
	//	break;
	}
}

//******************************************************************************
//
//		描画処理
//
//******************************************************************************

void SceneGame::draw(UINT i)
{
	switch (i)
	{
		case Enum::WINDOW_0:
			pStageManager->draw();			// ステージ描画
			break;
		case Enum::WINDOW_1:
			pStageManager2->draw();			// ステージ描画
			break;
		case Enum::WINDOW_2:
			pStageManager3->draw();

			break;
		default:
			break;
	}
	//pause.draw();
}

//******************************************************************************

void SceneGame::uninit(UINT i)
{
	switch (i)
	{
		case Enum::WINDOW_0:
			pStageManager->uninit();			// ステージ描画
			break;
		case Enum::WINDOW_1:
			pStageManager2->uninit();			// ステージ描画
			break;
		case Enum::WINDOW_2:
			pStageManager3->uninit();

			break;
		default:
			break;
	}
}
//******************************************************************************
//
//		ポーズ処理
//
//******************************************************************************

//------------------------------------------------------------------------------

//void Pause::init()
//{
//	state = 0;
//}
//
//int Pause::operator()()
//{
//	switch (state) {
//	case 0:
//		if (pad_trg & PAD_START) {
//			state = 1;
//		}
//		break;
//	case 1:
//		if (pad_trg & PAD_START) {
//			state = 0;
//			break;
//		}
//		if (comKonami(pad_trg)) {
//			OBJ2D* parent = &(*pPlayerManager->begin());
//			if (parent->mover) {
//				parent->hp = 8;
//				for (int i = 0; i < OPTION_MAX; i++) {
//					OBJ2D* option = pOptionManager->searchSet(option_move, parent->pos);
//					if (!option) break;
//					option->parent = parent;
//					option->index = i;
//					parent = option;
//				}
//				pOptionManager->update();				// 描画のため1回だけ呼び出す
//			}
//		}
//		break;
//	}
//
//	return state;
//}
//
//void Pause::draw()
//{
//	if (state) GL::DrawStringM(320, 180, "PAUSE", COLOR_WHITE, true);
//}

//******************************************************************************