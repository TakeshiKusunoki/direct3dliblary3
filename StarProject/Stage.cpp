//******************************************************************************
//
//
//		StageManager
//
//
//******************************************************************************
#include "All.h"
#include "BaseLib\\winMainFunc.h"
// ステージデータ


//******************************************************************************
//
//		StageManagerクラス
//
//******************************************************************************
UINT BaseStageManager::WINDOW_NUMBER = 0;
bool BaseStageManager::shot2Add = false;
UINT BaseStageManager::turn[PLAYERNUM] = { ENUM::PLAYER_TURN,ENUM::PLAYER_TURN };//どちらのターン
UINT BaseStageManager::turnNum = 0;//ターン番号
UINT BaseStageManager::point[PLAYERNUM] = { 0,0 };//ポイント
bool BaseStageManager::endFlag = false;//ターン終了フラグ


// 初期設定
void StageManager::init()
{
	state = 0;
	timer = 0;
	turn[0] = ENUM::PLAYER_TURN;
	turn[1] = ENUM::PLAYER_TURN;
	turnNum = 0;
	point[0] = 0;
	point[1] = 0;
	endFlag = false;

	pPlayerManager->init();
	pShotManager->init();
	pBuckManager->init();
	pBalyerManager->init();
}

//==============================================================================

// 更新処理
void StageManager::update()
{
	WINDOW_NUMBER = Enum::WINDOW_0;
	switch (state)
	{
		case 0://ステージに配置
			//	投影変換行列
			//projection = DirectX::XMMatrixOrthographicLH(16.0f, 9.0f, 0.1f, 1000.0f);
			//	光源(平行光)
			light_direction = DirectX::XMFLOAT4(-300, 500, -3, 0);		//	上+奥 から 下+前へのライト
			lightColor = { 0.995f,0.995f,0.999f,0.99f };
			nyutoralLightColor = { 0.20f,0.21f,0.20f,1 };
			pPlayerManager->init();
			pShotManager->init();
			pBuckManager->init();
			pBalyerManager->init();
			//キャラ追加
			{
				MoveAlgHelper* moveAlgHelper[1] = { nullptr };
				pPlayerManager->add(&charDefault, moveAlgHelper, DirectX::XMFLOAT3(0, 0, 0), Enum::WINDOW_0, ENUM::PLAYER_1,false);
				pPlayerManager->add(&charDefault, moveAlgHelper, DirectX::XMFLOAT3(0, 100, 0), Enum::WINDOW_0, ENUM::PLAYER_2, false);
				pPlayerManager->add(&charDefault, moveAlgHelper, DirectX::XMFLOAT3(-100, 0, 0), Enum::WINDOW_0, ENUM::PLAYER_2, false);
				pPlayerManager->add(&charDefault, moveAlgHelper, DirectX::XMFLOAT3(0, 0, 300), Enum::WINDOW_0, ENUM::PLAYER_NONE, false);

			}
			//背景追加
			{
				MoveAlgHelper* moveAlgHelper[1] = { nullptr };
				pBuckManager->add(&Tenkyu, moveAlgHelper, DirectX::XMFLOAT3(0, 0, 0), Enum::WINDOW_0, ENUM::PLAYER_2, false, DirectX::XMFLOAT3(10, 10, 10)/*, DirectX::XMFLOAT3(0, 90, 0)*/);
				pBuckManager->add(&Planet2, moveAlgHelper, DirectX::XMFLOAT3(-250, 0, -50), Enum::WINDOW_0, ENUM::PLAYER_1, false, DirectX::XMFLOAT3(0.05f, 0.05f, 0.05f));
				pBuckManager->add(&Planet3, moveAlgHelper, DirectX::XMFLOAT3(-200, -200, -50), Enum::WINDOW_0, ENUM::PLAYER_1, false, DirectX::XMFLOAT3(0.05f, 0.05f, 0.05f));
				//pBuckManager->add(&Utyu, moveAlgHelper, DirectX::XMFLOAT3(0, 0, 0), Enum::WINDOW_0, ENUM::PLAYER_1, false, DirectX::XMFLOAT3(0.001f, 0.001f, 0.001f));
			}
			//DestroyWindow(WinFunc::GetHwnd(1));
			state++;
		case 1://ゲーム部分

			switch (turn[ENUM::ENEMY_TURN])
			{
				case 0:
				default:
					break;
			}

			{
				static float ang = 0;
				light_direction.x = cosf(ToRadian(ang)) * 70;
				light_direction.y = -sinf(ToRadian(ang)) * 70;

				nyutoralLightColor.x = cosf(ToRadian(ang)) / 2 + 0.2f;
				nyutoralLightColor.y = sinf(ToRadian(ang)) / 2 + 0.2f;

				ang += 0.07f;
			}

			//light_direction.z;

			pPlayerManager->update();
			pShotManager->update();
			pBuckManager->update();
			pBalyerManager->update();

			//ターンが変わったら
			if (turn[ENUM::PLAYER_TURN] != ((turnNum+2) % 2))
			{
				printf("turn[ENUM::PLAYER_TURN]%d\n", turnNum);
				state++;
			}

			timer++;
			break;
		case 2:


			timer = 0;
			state++;
		case 3:

			pPlayerManager->update();
			pShotManager->update();
			pBuckManager->update();
			pBalyerManager->update();
			if (timer > SEC * 5)
			{
				turn[ENUM::PLAYER_TURN] = turnNum % 2;
				if (turn[ENUM::PLAYER_TURN] == ENUM::PLAYER_TURN)
					printf("自分の番\n");
				printf("%dの番\n", turn[ENUM::PLAYER_TURN]);

				state = 0;
			}
			timer++;
			break;
	}
}

//==============================================================================


// 描画処理
void StageManager::draw()
{
	// カメラクラス
	Camera camera;
	//	ビュー変換行列
	view = camera.GetView_();

	//	投影変換行列（平行投影）
	LONG width = WinFunc::GetScreenWidth();
	LONG height = WinFunc::GetScreenHeight();
	float aspect = (float)width / height;
	projection = camera.SetPerspective(30 * 0.01745f, aspect, 0.1f, 2000);
	//projection = camera.GetProjection();

	pPlayerManager->draw(view, projection, pSceneGame->GetTimer());
	pShotManager->draw(view, projection, pSceneGame->GetTimer());
	pBalyerManager->draw(view, projection, pSceneGame->GetTimer());
	pBuckManager->draw(view, projection, pSceneGame->GetTimer());

}



void StageManager::uninit()
{
}

//******************************************************************************






















//******************************************************************************
// window2
void StageManager2::init()
{
	state = 0;
	timer = 0;
	pShotManager2->init();
	pEnemyManager->init();
	pBuckManager2->init();
	pBalyerManager2->init();
}



void StageManager2::update()
{
	WINDOW_NUMBER = Enum::WINDOW_1;
	switch (state)
	{
		case 0://ステージに配置
			   //	投影変換行列
			   //projection = DirectX::XMMatrixOrthographicLH(16.0f, 9.0f, 0.1f, 1000.0f);
			   //	光源(平行光)
			light_direction = DirectX::XMFLOAT4(-300, 500, -3, 0);		//	上+奥 から 下+前へのライト
			lightColor = { 0.995f,0.995f,0.999f,0.99f };
			nyutoralLightColor = { 0.20f,0.21f,0.20f,1 };
			pShotManager2->init();
			pEnemyManager->init();
			pBuckManager2->init();
			pBalyerManager2->init();
			//キャラ追加
			{
				MoveAlgHelper* moveAlgHelper[1] = { nullptr };
				pEnemyManager->add(&charSaicoRed, moveAlgHelper, DirectX::XMFLOAT3(0, 10, 10), Enum::WINDOW_1, ENUM::PLAYER_2, true, DirectX::XMFLOAT3(0.5f, 0.5f, 0.5f));
				pEnemyManager->add(&charDefault, moveAlgHelper, DirectX::XMFLOAT3(0, 10, 200), Enum::WINDOW_1, ENUM::PLAYER_NONE, false, DirectX::XMFLOAT3(0.5f, 0.5f, 0.5f));
			}
			//背景
			{
				MoveAlgHelper* moveAlgHelper[1] = { nullptr };
				pBuckManager2->add(&Planet, moveAlgHelper, DirectX::XMFLOAT3(150, 50, -200), Enum::WINDOW_1, ENUM::PLAYER_1, false, DirectX::XMFLOAT3(0.05f, 0.05f, 0.05f));
				pBuckManager2->add(&Tenkyu, moveAlgHelper, DirectX::XMFLOAT3(0, 0, 0), Enum::WINDOW_1, ENUM::PLAYER_1, false, DirectX::XMFLOAT3(10, 10, 10), DirectX::XMFLOAT3(0, 90, 0));
			}

			state++;
		case 1://ゲーム部分
			if (shot2Add)
			{
				MoveAlgHelper* moveAlgHelper[1] = { nullptr };
				pShotManager2->add(&shotFall, moveAlgHelper, DirectX::XMFLOAT3(-100, 0, 0), Enum::WINDOW_1);
				shot2Add = false;
			}

			{
				static float ang = 0;
				light_direction.x = cosf(ToRadian(ang)) * 70;
				light_direction.y = -sinf(ToRadian(ang)) * 70;

				nyutoralLightColor.x = sinf(ToRadian(ang)) / 2 + 0.2f;
				nyutoralLightColor.y = cosf(ToRadian(ang)) / 2 + 0.2f;
				ang += 0.07f;
			}

			pShotManager2->update();
			pEnemyManager->update();
			pBuckManager2->update();
			pBalyerManager2->update();

			//ターンが変わったら
			if (turn[ENUM::ENEMY_TURN] != ((turnNum + 2) % 2))
			{
				state++;
			}

			timer++;
			break;
		case 2:

			timer = 0;
			state++;
		case 3:

			pEnemyManager->update();
			pShotManager2->update();
			pBuckManager2->update();
			pBalyerManager2->update();
			if (timer > SEC * 5)
			{
				turn[ENUM::ENEMY_TURN] = turnNum % 2;
				if (turn[ENUM::ENEMY_TURN] == ENUM::ENEMY_TURN)
					printf("自分の番\n");
				printf("%dの番\n", turn[ENUM::ENEMY_TURN]);

				state = 0;
			}
			timer++;
			break;
	}
}




void StageManager2::draw()
{
	Camera camera;
	//	ビュー変換行列
	//view = camera.GetView_();
	//DirectX::XMVECTOR position = DirectX::XMVectorSet(0.0f, 0.0f, 200.0f, 1.0f);	//	カメラの位置
	//DirectX::XMVECTOR target = DirectX::XMVectorSet(0.0f, 0.0f, 0.0f, 1.0f);		//	カメラの注視点
	//DirectX::XMVECTOR	up = DirectX::XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);		//	上ベクトル（仮）
	//view = DirectX::XMMatrixLookAtLH(position, target, up);

	view = camera.GetView_();
	//	投影変換行列（平行投影）
	LONG width = WinFunc::GetScreenWidth(Enum::WINDOW_1);
	LONG height = WinFunc::GetScreenHeight(Enum::WINDOW_1);
	float aspect = (float)width / height;
	projection = camera.SetPerspective(30 * 0.01745f, aspect, 0.1f, 2000);
	//projection = camera.GetProjection();

	pEnemyManager->draw(view, projection, pSceneGame->GetTimer());
	pShotManager2->draw(view, projection, pSceneGame->GetTimer());
	pBalyerManager2->draw(view, projection, pSceneGame->GetTimer());
	pBuckManager2->draw(view, projection, pSceneGame->GetTimer());

}

void StageManager2::uninit()
{
}












void StageManager3::init()
{
	state = 0;
	timer = 0;
	pBuckManager3->init();

	for (int i = 0; i < 100; i++)
	{
		spr[i] = nullptr;
	}
	spr[0] = new Sprite2D(GetDevice(), L"DATA\\picture\\hankyu_blue.png");
	spr[1] = new Sprite2D(GetDevice(), L"DATA\\picture\\Select.png");

}

void StageManager3::update()
{
	WINDOW_NUMBER = Enum::WINDOW_2;
	switch (state)
	{
		case 0://ステージに配置
			   //	投影変換行列
			   //projection = DirectX::XMMatrixOrthographicLH(16.0f, 9.0f, 0.1f, 1000.0f);
			   //	光源(平行光)
			light_direction = DirectX::XMFLOAT4(-300, 500, -3, 0);		//	上+奥 から 下+前へのライト
			//light_direction = DirectX::XMFLOAT4(0, -1, 1, 0);		//	上+奥 から 下+前へのライト
			lightColor = { 0.995f,0.995f,0.999f,0.99f };
			nyutoralLightColor = { 0.20f,0.21f,0.20f,1 };
			{
				MoveAlgHelper* moveAlgHelper[1] = { nullptr };
				pBuckManager3->add(&Utyu, moveAlgHelper, DirectX::XMFLOAT3(0, 0, 0), Enum::WINDOW_2, ENUM::PLAYER_2, false, DirectX::XMFLOAT3(0.02f, 0.02f, 0.02f));
			}

			state++;
		case 1://ゲーム部分
		/*	if (timer%SEC == 0)
			{
				MoveAlgHelper* moveAlgHelper[1] = { nullptr };
				pBuckManager3->add(&shotFall, moveAlgHelper, DirectX::XMFLOAT3(-light_direction.x, -light_direction.y, -light_direction.z));

			}*/
			static float ang = 0;
			light_direction.x = cosf(ToRadian(ang)) * 70;
			light_direction.y = -sinf(ToRadian(ang)) * 70;
			ang += 0.07f;
			pBuckManager3->update();

			//当たり判定
			Judge();

			//ターン終了フラグ
			if (endFlag)
			{
				endFlag = false;
				turnNum++;
			}


			timer++;
			break;
	}
}

void StageManager3::draw()
{
	// カメラクラス
	Camera camera;
	//	ビュー変換行列
	static DirectX::XMFLOAT3 base(0.0f, 50.0f, 100.0f);
	static DirectX::XMFLOAT3 tBase(0.0f, 0.0f, 0.0f);
	DirectX::XMVECTOR	up;
	DirectX::XMVECTOR position;//位置
	DirectX::XMVECTOR target;//注視点
	//DirectX::XMMATRIX projection;//	投影行列 キャッシュ用

	DirectX::XMFLOAT4 cameraPos;
	cameraPos = { base.x, base.y, base.z, 1.0f };
	position = DirectX::XMLoadFloat4(&cameraPos);

	target = DirectX::XMVectorSet(tBase.x, tBase.y, tBase.z, 1.0f);
	up = DirectX::XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);

	view = DirectX::XMMatrixLookAtLH(position, target, up);
	//view = camera.GetView_();

	//	投影変換行列（平行投影）
	LONG width = WinFunc::GetScreenWidth(Enum::WINDOW_2);
	LONG height = WinFunc::GetScreenHeight(Enum::WINDOW_2);
	float aspect = (float)width / height;
	projection = camera.SetPerspective(30 * 0.01745f, aspect, 0.1f, 800);

	pBuckManager3->draw(view, projection, pSceneGame->GetTimer());
	//spr[0]->Render2(GetDeviceContext(), 50, 60, 0, 0, 1024, 1024, static_cast<float>(WinFunc::GetScreenWidth(Enum::WINDOW_2)) / 1024, static_cast<float>(WinFunc::GetScreenHeight(Enum::WINDOW_2)) / 1024);
	/*if (GetAsyncKeyState('1'))
	{
		spr[1]->Render2(GetDeviceContext(), 0, 0, 0, 0, 1520, 1280, static_cast<float>(WinFunc::GetScreenWidth(Enum::WINDOW_2)) / 1520, static_cast<float>(WinFunc::GetScreenHeight(Enum::WINDOW_2)) / 1280);
	}*/
}

void StageManager3::uninit()
{
	for (int i = 0; i < 100; i++)
	{
		if (spr[i])delete[] spr[i];
		spr[i] = nullptr;
	}
}












//if (input::TRG(0) &  input::PAD_R1)
//{
//	lightColor.x = (float)(rand() % 50) / 100 + 0.5f;
//	lightColor.y = (float)(rand() % 50) / 100 + 0.5f;
//	lightColor.z = (float)(rand() % 50) / 100 + 0.5f;
//}
//if (input::TRG(0) &  input::PAD_TRG4)
//{
//	nyutoralLightColor.x = (float)(rand() % 4) / 10 + 0.1f;
//	nyutoralLightColor.y = (float)(rand() % 4) / 10 + 0.1f;
//	nyutoralLightColor.z = (float)(rand() % 4) / 10 + 0.1f;
//}
//if (input::TRG(0) &  input::PAD_SELECT)
//{
//	light_direction.x = (float)(rand()) - (float)(rand());
//	light_direction.y = (float)(rand()) - (float)(rand());
//	light_direction.z = (float)(rand()) - (float)(rand());
//}