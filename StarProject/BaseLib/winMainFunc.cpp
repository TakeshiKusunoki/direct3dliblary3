#include <stdio.h>
#include <tchar.h>
#include "winMainFunc.h"
#include <assert.h>




#define WINDOWS_CLASS_NAME TEXT("Win.Sample.WindowClass")//ウインドウクラスにつける名前
#define WINDOWS_CLASS_NAME2 TEXT("Win.Sample.WindowClass2")//ウインドウクラスにつける名前
#define WINDOWS_CLASS_NAME3 TEXT("Win.Sample.WindowClass3")//ウインドウクラスにつける名前



static HWND hWnd[WINDOW_NUM];//画面１のウインドウハンドル



// ほかのCPPでも使われる定数
const UINT ScreenWidth[WINDOW_NUM] = {640, 640 , 1280};//画面幅
const UINT ScreenHeight[WINDOW_NUM] = { 760, 760 , 760 };//画面高さ
static high_resolution_timer Timer[WINDOW_NUM];//フレームに影響されないタイマー
static POINT	m_cursorPos;//カーソル位置






//==============================================================================
//
//		メッセージ処理
//
//==============================================================================

// メッセージループ
bool WinFunc::MsgLoop()
{
	MSG msg = {};


	while (PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
	{
		if (msg.message == WM_QUIT) return false;
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	////フレームレート用
	//while (!m.hrTimer.tick());
	//if (isShowFrameRate)
	//{
	//	calculateFrameStats(m.hwnd, &m.hrTimer);//フレームレート計算・タイトルバーに表示
	//}
	//return static_cast<int>(msg.wParam);// メッセージループ終了（ゲーム処理へ）
	return true;

}


//==============================================================================

// ウインドウの終了処理
void WinFunc::Uninit()
{
	UnregisterClass(WINDOWS_CLASS_NAME, GetModuleHandle(NULL));
	UnregisterClass(WINDOWS_CLASS_NAME2, GetModuleHandle(NULL));
	UnregisterClass(WINDOWS_CLASS_NAME3, GetModuleHandle(NULL));
}


//=============================================================================
//  関数名　：InitWindow
//  機能概要：表示するウィンドウの定義、登録、表示
//  戻り値　：正常終了のとき１、以上終了のとき０
//=============================================================================
BOOL WinFunc::InitWindow(HINSTANCE hInstance, int nShowCmd) {
	// ウインドウクラスを定義する
	WNDCLASSEX WindowClass;
	WindowClass.style = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS | CS_CLASSDC/*|CS_GLOBALCLASS| CS_DBLCLKS*/;//このクラスのウインドウ同士で１つのデバイスコンテキストを共有する
	WindowClass.lpfnWndProc = WinProc;								  //ウィンドウプロシージャ関数
	WindowClass.cbSize = sizeof(WNDCLASSEX);
	WindowClass.cbClsExtra = 0;									  //エキストラ（なしに設定）
	WindowClass.cbWndExtra = 0;									  //必要な情報（なしに設定）
	WindowClass.hInstance = hInstance;								  //このインスタンスへのハンドル
	WindowClass.hIcon = LoadIcon(hInstance, IDI_APPLICATION);		  //ラージアイコン
	WindowClass.hIconSm = LoadIcon(hInstance, IDI_WINLOGO);			 //スモールアイコン
	WindowClass.hCursor = LoadCursor(NULL, IDC_ARROW);				  //カーソルスタイル
	WindowClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);	  //ウィンドウの背景（黒に設定）
	WindowClass.lpszMenuName = NULL;								  //メニュー（なしに設定）
	WindowClass.lpszClassName = WINDOWS_CLASS_NAME;					 //このウインドウクラスにつける名前

	// ウインドウクラスを登録する
	if (!RegisterClassEx(&WindowClass)) {
		OutputDebugString(TEXT("Error: ウィンドウクラスの登録ができません。\n"));
		return false;
	}
	return (TRUE);

}

// 2番目
BOOL WinFunc::InitWindow2(HINSTANCE hInstance, int nShowCmd) {
	// ウインドウクラスを定義する
	WNDCLASSEX WindowClass;
	WindowClass.style = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS | CS_CLASSDC/*|CS_GLOBALCLASS| CS_DBLCLKS*/;//このクラスのウインドウ同士で１つのデバイスコンテキストを共有する
	WindowClass.lpfnWndProc = WinProc2;								  //ウィンドウプロシージャ関数
	WindowClass.cbSize = sizeof(WNDCLASSEX);
	WindowClass.cbClsExtra = 0;									  //エキストラ（なしに設定）
	WindowClass.cbWndExtra = 0;									  //必要な情報（なしに設定）
	WindowClass.hInstance = hInstance;								  //このインスタンスへのハンドル
	WindowClass.hIcon = LoadIcon(hInstance, IDI_APPLICATION);		  //ラージアイコン
	WindowClass.hIconSm = LoadIcon(hInstance, IDI_WINLOGO);			 //スモールアイコン
	WindowClass.hCursor = LoadCursor(NULL, IDC_ARROW);				  //カーソルスタイル
	WindowClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);	  //ウィンドウの背景（黒に設定）
	WindowClass.lpszMenuName = NULL;								  //メニュー（なしに設定）
	WindowClass.lpszClassName = WINDOWS_CLASS_NAME2;					 //このウインドウクラスにつける名前

	// ウインドウクラスを登録する
	if (!RegisterClassEx(&WindowClass)) {
		OutputDebugString(TEXT("Error: ウィンドウクラスの登録ができません。\n"));
		return false;
	}



	return (TRUE);
}



// 3番目
BOOL WinFunc::InitWindow3(HINSTANCE hInstance, int nShowCmd)
{
	// ウインドウクラスを定義する
	WNDCLASSEX WindowClass;
	WindowClass.style = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS | CS_CLASSDC/*|CS_GLOBALCLASS| CS_DBLCLKS*/;//このクラスのウインドウ同士で１つのデバイスコンテキストを共有する
	WindowClass.lpfnWndProc = WinProc3;								  //ウィンドウプロシージャ関数
	WindowClass.cbSize = sizeof(WNDCLASSEX);
	WindowClass.cbClsExtra = 0;									  //エキストラ（なしに設定）
	WindowClass.cbWndExtra = 0;									  //必要な情報（なしに設定）
	WindowClass.hInstance = hInstance;								  //このインスタンスへのハンドル
	WindowClass.hIcon = LoadIcon(hInstance, IDI_APPLICATION);		  //ラージアイコン
	WindowClass.hIconSm = LoadIcon(hInstance, IDI_WINLOGO);			 //スモールアイコン
	WindowClass.hCursor = LoadCursor(NULL, IDC_ARROW);				  //カーソルスタイル
	WindowClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);	  //ウィンドウの背景（黒に設定）
	WindowClass.lpszMenuName = NULL;								  //メニュー（なしに設定）
	WindowClass.lpszClassName = WINDOWS_CLASS_NAME3;					 //このウインドウクラスにつける名前

	// ウインドウクラスを登録する
	if (!RegisterClassEx(&WindowClass))
	{
		OutputDebugString(TEXT("Error: ウィンドウクラスの登録ができません。\n"));
		return false;
	}



	return (TRUE);
}



/////////////////////////////////////////
// プロットウィンドウの作成
/////////////////////////////////////////
BOOL WinFunc::InitInstance(HINSTANCE hInstance, int nShowCmd) {
	// ウインドウクラスの登録ができたので、ウィンドウを生成する。

	hWnd[Enum::WINDOW_0] = CreateWindowEx(
		WS_EX_ACCEPTFILES | WS_EX_CLIENTEDGE | WS_EX_LAYERED,			   //拡張ウィンドウスタイル
		WINDOWS_CLASS_NAME,								//ウィンドウクラスの名前
		TEXT("☆Star Project"),							//ウィンドウタイトル
		WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_SYSMENU,	   //ウィンドウスタイル
		940,												//
		0,												//
		ScreenWidth[Enum::WINDOW_0],									   //
		ScreenHeight[Enum::WINDOW_0],									   //
		NULL,											//親ウィンドウ（なし）
		NULL,											//メニュー（なし）
		hInstance,										//このプログラムのインスタンスのハンドル
		NULL											   //追加引数（なし）
	);
	if (!hWnd[Enum::WINDOW_0])
	{
		OutputDebugString(TEXT("Error: ウィンドウが作成できません。\n"));
		return false;
	}

	ShowWindow(hWnd[Enum::WINDOW_0], nShowCmd); // ウィンドウを表示する
	UpdateWindow(hWnd[Enum::WINDOW_0]);

	return (TRUE);
}

// 2番目
// プロットウィンドウの作成
BOOL WinFunc::InitInstance2(HINSTANCE hInstance, int nShowCmd) {
	// ウインドウクラスの登録ができたので、ウィンドウを生成する。

	hWnd[Enum::WINDOW_1] = CreateWindowEx(
		WS_EX_ACCEPTFILES | WS_EX_CLIENTEDGE | WS_EX_LAYERED,			   //拡張ウィンドウスタイル
		WINDOWS_CLASS_NAME2,								//ウィンドウクラスの名前
		TEXT("☆Star Project2"),							//ウィンドウタイトル
		WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_SYSMENU,	   //ウィンドウスタイル
		300,												//
		0,												//
		ScreenWidth[Enum::WINDOW_1],									   //
		ScreenHeight[Enum::WINDOW_1],									   //
		NULL,											//親ウィンドウ（なし）
		NULL,											//メニュー（なし）
		hInstance,										//このプログラムのインスタンスのハンドル
		NULL											   //追加引数（なし）
	);
	if (!hWnd[Enum::WINDOW_1])
	{
		OutputDebugString(TEXT("Error: ウィンドウが作成できません。\n"));
		return false;
	}

	ShowWindow(hWnd[Enum::WINDOW_1], nShowCmd); // ウィンドウを表示する
	UpdateWindow(hWnd[Enum::WINDOW_1]);
	return (TRUE);
}








// 3番目
// プロットウィンドウの作成
BOOL WinFunc::InitInstance3(HINSTANCE hInstance, int nShowCmd)
{
	// ウインドウクラスの登録ができたので、ウィンドウを生成する。

	hWnd[Enum::WINDOW_2] = CreateWindowEx(
		WS_EX_ACCEPTFILES | WS_EX_CLIENTEDGE | WS_EX_LAYERED,			   //拡張ウィンドウスタイル
		WINDOWS_CLASS_NAME3,								//ウィンドウクラスの名前
		TEXT("☆Star Project3"),							//ウィンドウタイトル
		WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_SYSMENU,	   //ウィンドウスタイル
		300,												//
		20,												//
		ScreenWidth[Enum::WINDOW_2],									   //
		ScreenHeight[Enum::WINDOW_2],									   //
		NULL,											//親ウィンドウ（なし）
		NULL,											//メニュー（なし）
		hInstance,										//このプログラムのインスタンスのハンドル
		NULL											   //追加引数（なし）
	);
	if (!hWnd[Enum::WINDOW_2])
	{
		OutputDebugString(TEXT("Error: ウィンドウが作成できません。\n"));
		return false;
	}

	ShowWindow(hWnd[Enum::WINDOW_2], nShowCmd); // ウィンドウを表示する
	UpdateWindow(hWnd[Enum::WINDOW_2]);
	return (TRUE);
}














//
//#include <atlbase.h>
//#include <atlwin.h>
//#include <atltypes.h>
//#include <atlcom.h>
//#include <tchar.h>
//#include <dwmapi.h>
//#include <list>
//
//#pragma comment (lib, "dwmapi.lib")

//HRESULT EnableBlurBehind(HWND hwnd)
//{
//	HRESULT hr = S_OK; // Create and populate the Blur Behind structure
//	DWM_BLURBEHIND bb = { 0 };
//	// Enable Blur Behind and apply to the entire client area
//	bb.dwFlags = DWM_BB_ENABLE;
//	bb.fEnable = true;
//	bb.hRgnBlur = NULL;
//	//Apply Blur Behind
//	CreateSolidBrush(0x00000000);
//	hr = DwmEnableBlurBehindWindow(hwnd, &bb);
//	if (SUCCEEDED(hr))
//	{ // ...
//		return hr;
//	}
//	return hr;
//}
//HBRUSH m_brush;
//HFONT m_font;
//const int BottomRegionSize = 190;
//LRESULT OnDestroy(UINT msg, WPARAM wParam, LPARAM lParam, BOOL& isHandled)
//{
//	DeleteObject(m_brush);
//	m_brush = NULL;
//	DeleteObject(m_font);
//	m_font = NULL;
//	PostQuitMessage(0);
//	return 0;
//}
//
//HRESULT EnableBlurBehind(HWND hWnd)
//{
//	HRESULT hr = S_OK;
//
//	DWM_BLURBEHIND bb = { 0 };
//	bb.dwFlags = DWM_BB_ENABLE | DWM_BB_BLURREGION;
//	bb.fEnable = true;
//
//	{
//		BOOL ret;
//		HDC hdc = CreateCompatibleDC(NULL);
//		HBITMAP bmp = CreateCompatibleBitmap(NULL, 640, 480);
//		HBITMAP oldBmp = (HBITMAP)SelectObject(hdc, bmp);
//		HFONT oldFont = (HFONT)SelectObject(hdc, m_font);
//		SetBkMode(hdc, TRANSPARENT);
//		SetPolyFillMode(hdc, ALTERNATE);
//
//		ret = BeginPath(hdc);
//		ret = TextOutW(hdc, 10, 300, L"Windows Vista", wcslen(L"Windows Vista"));
//		ret = EndPath(hdc);
//		BYTE type[1024] = { 0 };
//		POINT points[1024];
//		int num = GetPath(hdc, points, type, 1024);
//		HRGN rgn = PathToRegion(hdc);
//
//		HRGN rgn2 = CreateEllipticRgn(160, 40, 480, 360);
//		int error = CombineRgn(rgn, rgn2, rgn, RGN_XOR);
//
//		SelectObject(hdc, oldBmp);
//		SelectObject(hdc, oldFont);
//		DeleteObject(bmp);
//		DeleteObject(rgn2);
//		DeleteDC(hdc);
//
//		bb.hRgnBlur = rgn;
//	}
//
//	hr = DwmEnableBlurBehindWindow(hWnd, &bb);
//
//	MARGINS mgn = { -1,0,0,0 };
//	mgn.cyBottomHeight = BottomRegionSize;
//	hr = DwmExtendFrameIntoClientArea(hWnd, &mgn);
//	return hr;
//}
#include <math.h>
//=============================================================================
//      ウィンドウプロシジャ関数（WindowProcedure）
//=============================================================================
LRESULT CALLBACK WinFunc::WinProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
#ifdef _DEBUG//デバッグテスト
	TCHAR debugText[1024];
	wsprintf(debugText, TEXT("uMsg=%d, wParam=%d, lParam=%d\n"), message, wParam, lParam);
	OutputDebugString(debugText);
#endif
	switch (message)
	{
		case WM_PAINT:
			{
				PAINTSTRUCT ps;
				HDC hdc;
				hdc = BeginPaint(hWnd, &ps);
				//hBrush = CreateSolidBrush(RGB(255, 0, 0));
				//SelectObject(hdc, hBrush);
				//ExtFloodFill(hdc, 1, 1, RGB(255, 255, 255), FLOODFILLSURFACE);
				EndPaint(hWnd, &ps);
				break;
			}
		case WM_LBUTTONDOWN:
			printf("window1\n");
#define PI 3.1415926535
			for (double y = PI; y >= -PI; y -= 0.2)
			{
				for (double x = -PI; x <= PI; x += 0.1)
				{
					double r = sqrt(x * x + y * y);
					double th = atan2(y, x);

					printf("%c", r <= 1.0 / cos((acos(sin(5.0 * th)) - 2.0 * PI) * 0.2) ? '*' : ' ');
				}
				printf("\n");
			}

			break;
		case WM_DESTROY:
			//PostQuitMessage(0);
			break;
		case WM_CREATE:
			//SetLayeredWindowAttributes(hWnd, RGB(255, 0, 0), 0, LWA_COLORKEY);
			//EnableBlurBehind(hWnd);
			SetLayeredWindowAttributes(hWnd, 0, 128, LWA_ALPHA);
			break;
		case WM_CLOSE:
			{
				int messageResult = MessageBox(
					hWnd, TEXT("ウィンドウを閉じます。\nよろしいですか？"), TEXT("確認"),
					MB_OKCANCEL | MB_ICONWARNING
				);

				if (messageResult != IDOK) return 0;
				else DefWindowProc(hWnd, message, wParam, lParam);
			}
			break;
		case WM_KEYDOWN:
			//if (wParam == VK_ESCAPE) PostMessage(hWnd, WM_CLOSE, 0, 0);
			if (wParam == VK_ESCAPE)
			{
				PostQuitMessage(0);
			}
			break;
		case WM_ENTERSIZEMOVE:
			// WM_EXITSIZEMOVE is sent when the user grabs the resize bars.
			Timer[Enum::WINDOW_0].stop();
			break;
		case WM_EXITSIZEMOVE:
			// WM_EXITSIZEMOVE is sent when the user releases the resize bars.
			// Here we reset everything based on the new window dimensions.
			Timer[Enum::WINDOW_0].start();
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
	}

	return 0;

}




//   （WindowProcedure2）
LRESULT CALLBACK WinFunc::WinProc2(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
#ifdef _DEBUG//デバッグテスト
	TCHAR debugText[1024];
	wsprintf(debugText, TEXT("uMsg=%d, wParam=%d, lParam=%d\n"), message, wParam, lParam);
	OutputDebugString(debugText);
#endif
	switch (message)
	{
	//case WM_DESTROY: // 閉じるボタンをクリックした時
	//	PostQuitMessage(0); // WM_QUITメッセージを発行
	//	break;
	case WM_LBUTTONDOWN:
		printf("window2\n");
		printf("　　 ＿　　_,、- 、\n");
		printf("／　　`´　　　　ヽ\n");
		printf("/ , -､ 　’’　　　 ‘, \n");
		printf("{ `ｰ’　　　　 / :::: }\n");
		printf("`ｰ - _　　　　, ’:::::::::, ’\n");
		printf("`i　 r‐{ ::::::: / \n");

		break;
	case WM_CREATE:
		//SetLayeredWindowAttributes(hWnd, RGB(255, 0, 0), 0, LWA_COLORKEY);
		SetLayeredWindowAttributes(hWnd, 0, 128, LWA_ALPHA);
		break;
	case WM_CLOSE:
		{
			int messageResult = MessageBox(
				hWnd, TEXT("ウィンドウを閉じます。\nよろしいですか？"), TEXT("確認"),
				MB_OKCANCEL | MB_ICONWARNING
			);

			if (messageResult != IDOK) return 0;
			else DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_KEYDOWN:
		if (wParam == VK_ESCAPE)
		{
			PostQuitMessage(0);
		}
		break;
	default: // 上記以外のメッセージはWindowsへ処理を任せる
		return DefWindowProc(hWnd, message, wParam, lParam);
	}

	return 0;

}




#include <dshow.h>
#pragma comment(lib, "Strmiids.lib")
//   （WindowProcedure3）
LRESULT CALLBACK WinFunc::WinProc3(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
#ifdef _DEBUG//デバッグテスト
	TCHAR debugText[1024];
	wsprintf(debugText, TEXT("uMsg=%d, wParam=%d, lParam=%d\n"), message, wParam, lParam);
	OutputDebugString(debugText);
#endif
	switch (message)
	{
		case WM_PAINT:
			{
				PAINTSTRUCT ps;
				HDC hdc;
				hdc = BeginPaint(hWnd, &ps);
				EndPaint(hWnd, &ps);
				break;
			}
		/*case WM_DESTROY:
			PostQuitMessage(0);
			break;*/
		/*case WM_RBUTTONUP:
			DestroyWindow(hWnd);
			PostQuitMessage(0);
			return 0;*/
		case WM_LBUTTONDOWN:
			printf("Buck\n");
			int i, j;


			for (i = 1; i <= 10; i++)
			{
				for (j = i; j<10; j++)
					printf(" ");
				for (j = 1; j <= (i * 2 - 1); j++)
					printf("*");

				printf("\n");
			}
			for (int j = 1; j <= 10; ++j)
			{
				//星を表示する座標を決める
				if (j % 2 == 0) printf("☆");
				else printf("★");
			}
				printf("\n");

			break;
		case WM_CLOSE:
			{
				int messageResult = MessageBox(
					hWnd, TEXT("ウィンドウを閉じます。\nよろしいですか？"), TEXT("確認"),
					MB_OKCANCEL | MB_ICONWARNING
				);

				if (messageResult != IDOK) return 0;
				else DefWindowProc(hWnd, message, wParam, lParam);
			}
			break;
		case WM_CREATE:
			SetLayeredWindowAttributes(hWnd, 0, 255, LWA_ALPHA);
			break;
		case WM_KEYDOWN:
			if (wParam == VK_ESCAPE)
			{
				PostQuitMessage(0);
			}
			break;
		case WM_ENTERSIZEMOVE:
			// WM_EXITSIZEMOVE is sent when the user grabs the resize bars.
			Timer[Enum::WINDOW_2].stop();
			break;
		case WM_EXITSIZEMOVE:
			// WM_EXITSIZEMOVE is sent when the user releases the resize bars.
			// Here we reset everything based on the new window dimensions.
			Timer[Enum::WINDOW_2].start();
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
	}

	return 0;
}













//=============================================================================
//  機能概要：コンソール画面の表示
//　引数		：
//  戻り値　：正常終了のとき１、以上終了のとき０
//=============================================================================
BOOL WinFunc::Console()
{
	// コンソールを作成する
	AllocConsole();
	// 標準入出力に割り当てる
	FILE* fp = NULL;
	// 昔のコード
	// 現在のコード
	freopen_s(&fp, "CONOUT$", "w", stdout);
	freopen_s(&fp, "CONIN$", "r", stdin);
	printf("hello world\n");
	//FreeConsole();
	return (TRUE);
}

//==============================================================================
//
//		その他
//
//==============================================================================
// ゲッター
//ウインドウハンドルゲット
HWND WinFunc::GetHwnd(UINT i)
{
	if (i >= WINDOW_NUM)
	{
		assert(!"引数を画面の数に収めてください");
	}
	return hWnd[i];
}

//ScreenWidth
UINT WinFunc::GetScreenWidth(UINT i)
{
	if (i >= WINDOW_NUM)
	{
		assert(!"引数を画面の数に収めてください");
	}
	return	ScreenWidth[i];
}

//SCREEN_HEIGHT
UINT WinFunc::GetScreenHeight(UINT i)
{
	if (i >= WINDOW_NUM)
	{
		assert(!"引数を画面の数に収めてください");
	}
	return	ScreenHeight[i];
}




// カーソル表示
void WinFunc::CursorOn()
{
	while (ShowCursor(TRUE) < 0) {}
}
// カーソル消去
void WinFunc::CursorOff()
{
	while (ShowCursor(FALSE) >= 0) {}
}

//==============================================================================

// カーソル座標の更新
void WinFunc::UpdateCursorPos(UINT i)
{
	if (i >= WINDOW_NUM)
	{
		assert(!"引数を画面の数に収めてください");
	}
	GetCursorPos(&m_cursorPos);
	ScreenToClient(hWnd[i], &m_cursorPos);
}
// カーソル座標（クライアント座標）を返す
POINT WinFunc::GetCursorPos()
{
	return m_cursorPos;
}



#include <sstream>
//elapsedタイマーしゅとく
high_resolution_timer* WinFunc::GetElapsedTimer(UINT i)
{
	if (i >= WINDOW_NUM)
	{
		assert(!"引数を画面の数に収めてください");
	}
	return &Timer[i];
}
//// fps表示
//void  WinFunc::Calculate_frame_stats(UINT i)
//{
//	if (i >= WINDOW_NUM)
//	{
//		assert(!"引数を画面の数に収めてください");
//	}
//	// Code computes the average frames per second, and also the
//	// average time it takes to render one frame.  These stats
//	// are appended to the window caption bar.
//	static int frames[WINDOW_NUM] = { 0 };
//	static float time_tlapsed[WINDOW_NUM] = { 0.0f };
//
//	frames[i]++;
//
//	// Compute averages over one second period.
//	if ((Timer[i].time_stamp() - time_tlapsed[i]) >= 1.0f)
//	{
//		float fps = static_cast<float>(frames[i]); // fps = frameCnt / 1
//		float mspf = 1000.0f / fps;
//		std::ostringstream outs;
//		outs.precision(6);
//		outs << "FPS : " << fps << " / " << "Frame Time : " << mspf << " (ms)";
//		SetWindowTextA(hWnd[i], outs.str().c_str());
//
//		// Reset for next average.
//		frames[i] = 0;
//		time_tlapsed[i] += 1.0f;
//	}
//}

//******************************************************************************

////-----------------------------------------------------------------------------
//// Windowクラス
////-----------------------------------------------------------------------------
//class CMyWindow : public CWindowImpl<CMyWindow>
//{
//public:
//
//	BEGIN_MSG_MAP(CGameWindow)
//		MESSAGE_HANDLER(WM_SYSCOMMAND, OnSysCommand)
//		MESSAGE_HANDLER(WM_NCHITTEST, OnNCHitTest)
//		MESSAGE_HANDLER(WM_CREATE, OnCreate)
//		MESSAGE_HANDLER(WM_KEYDOWN, OnKeyDown)
//		MESSAGE_HANDLER(WM_DESTROY, OnDestroy)
//		MESSAGE_HANDLER(WM_ERASEBKGND, OnEraseBkgnd)
//	END_MSG_MAP()
//
//	static const int BottomRegionSize = 90;
//
//	CMyWindow()
//	{
//	}
//
//	LRESULT OnCreate(UINT msg, WPARAM wParam, LPARAM lParam, BOOL& isHandled)
//	{
//		m_brush = (HBRUSH)CreateSolidBrush(0x00000000);
//		m_font = CreateFontW(128,
//			0,
//			310,
//			0,
//			FW_BOLD,
//			FALSE,
//			FALSE,
//			FALSE,
//			DEFAULT_CHARSET,
//			OUT_DEFAULT_PRECIS,
//			CLIP_DEFAULT_PRECIS,
//			PROOF_QUALITY,
//			0,
//			L"メイリオ");
//		EnableBlurBehind();
//		return 0;
//	}
//
//	//-------------------------------------------------------------------------
//	// OnNCHitTest handler
//	//-------------------------------------------------------------------------
//	LRESULT OnNCHitTest(UINT msg, WPARAM wParam, LPARAM lParam, BOOL& isHandled)
//	{
//		isHandled = true;
//		return HTCAPTION;
//	}
//
//	//-------------------------------------------------------------------------
//	// OnSysCommand handler
//	//-------------------------------------------------------------------------
//	LRESULT OnSysCommand(UINT msg, WPARAM wParam, LPARAM lParam, BOOL& isHandled)
//	{
//		switch (wParam & 0xFFF0)
//		{
//			case SC_SCREENSAVE:
//			case SC_MONITORPOWER:
//				// スクリーンセーバ & 省電力モード避け
//				return 0;
//		}
//		// DefaultWindowProc に流す場合は，isHandled を false にする
//		isHandled = false;
//		return 0;
//	}
//
//	//-------------------------------------------------------------------------
//	// OnKeyDown handler
//	//-------------------------------------------------------------------------
//	LRESULT OnKeyDown(UINT msg, WPARAM wParam, LPARAM lParam, BOOL& isHandled)
//	{
//		// ESC で終了
//		if (wParam == VK_ESCAPE)
//		{
//			DestroyWindow();
//		}
//		return 0;
//	}
//
//	//-------------------------------------------------------------------------
//	// OnDestroy handler
//	//-------------------------------------------------------------------------
//	LRESULT OnDestroy(UINT msg, WPARAM wParam, LPARAM lParam, BOOL& isHandled)
//	{
//		DeleteObject(m_brush);
//		m_brush = NULL;
//		DeleteObject(m_font);
//		m_font = NULL;
//		PostQuitMessage(0);
//		return 0;
//	}
//	HBRUSH m_brush;
//	HFONT m_font;
//	//-------------------------------------------------------------------------
//	// OnEraseBkgnd handler
//	//-------------------------------------------------------------------------
//	LRESULT OnEraseBkgnd(UINT msg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
//	{
//		HDC hdc = (HDC)wParam;
//
//		CRect rect;
//		GetClientRect(&rect);
//		FillRect(hdc, &rect, m_brush);
//
//		bHandled = true;
//		return 1;
//	}
//
//	HRESULT EnableBlurBehind()
//	{
//		HRESULT hr = S_OK;
//
//		DWM_BLURBEHIND bb = { 0 };
//		bb.dwFlags = DWM_BB_ENABLE | DWM_BB_BLURREGION;
//		bb.fEnable = true;
//
//		{
//			BOOL ret;
//			HDC hdc = CreateCompatibleDC(NULL);
//			HBITMAP bmp = CreateCompatibleBitmap(NULL, 640, 480);
//			HBITMAP oldBmp = (HBITMAP)SelectObject(hdc, bmp);
//			HFONT oldFont = (HFONT)SelectObject(hdc, m_font);
//			SetBkMode(hdc, TRANSPARENT);
//			SetPolyFillMode(hdc, ALTERNATE);
//
//			ret = BeginPath(hdc);
//			ret = TextOutW(hdc, 10, 300, L"Windows Vista", wcslen(L"Windows Vista"));
//			ret = EndPath(hdc);
//			BYTE type[1024] = { 0 };
//			POINT points[1024];
//			int num = GetPath(hdc, points, type, 1024);
//			HRGN rgn = PathToRegion(hdc);
//
//			HRGN rgn2 = CreateEllipticRgn(160, 40, 480, 360);
//			int error = CombineRgn(rgn, rgn2, rgn, RGN_XOR);
//
//			SelectObject(hdc, oldBmp);
//			SelectObject(hdc, oldFont);
//			DeleteObject(bmp);
//			DeleteObject(rgn2);
//			DeleteDC(hdc);
//
//			bb.hRgnBlur = rgn;
//		}
//
//		hr = DwmEnableBlurBehindWindow(m_hWnd, &bb);
//
//		MARGINS mgn = { 0 };
//		mgn.cyBottomHeight = BottomRegionSize;
//		hr = DwmExtendFrameIntoClientArea(m_hWnd, &mgn);
//		return hr;
//	}
//
//	//-------------------------------------------------------------------------
//	// ProcessMessage
//	//-------------------------------------------------------------------------
//	bool ProcessMessage(WPARAM& uiRetCode)
//	{
//		MSG msg;
//		ZeroMemory(&msg, sizeof(MSG));
//		while (::PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
//		{
//			if (WM_QUIT == msg.message)
//			{
//				uiRetCode = msg.wParam;
//				return false;
//			}
//			::TranslateMessage(&msg);
//			::DispatchMessage(&msg);
//		}
//		return true;
//	}
//};

