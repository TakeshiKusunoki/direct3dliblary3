//******************************************************************************
//
//
//		タイトル処理
//
//
//******************************************************************************

#include "All.h"

//******************************************************************************
//
//		初期設定
//
//******************************************************************************

void SceneTitle::init(UINT i )
{
	timer = 0;
	switch (i)
	{
		case Enum::WINDOW_0:
			break;
		case Enum::WINDOW_1:
			break;
		case Enum::WINDOW_2:
			spr[0] = new Sprite2D(GetDevice(), L"DATA\\picture\\Title.png");
			break;
		default:
			break;
	}


}

//******************************************************************************
//
//		更新処理
//
//******************************************************************************

void SceneTitle::update(UINT i )
{
	switch (i)
	{
		case Enum::WINDOW_0:
			timer++;
			break;
		case Enum::WINDOW_1:
			break;
		case Enum::WINDOW_2:
			break;
		default:
			break;
	}

	if (input::TRG(0) & input::PAD_START) { 
		setScene(pSceneGame); 
	}
	if(GetAsyncKeyState('Z')<0) setScene(pSceneGame);
	//if (timer > SEC * 5)setScene(pSceneGame);
	// 5秒後にデモへ切り換え
	//if (timer > 60 * 5)setScene(pSceneDemo);

}

//******************************************************************************
//
//		描画処理
//
//******************************************************************************

void SceneTitle::draw(UINT i)
{
	switch (i)
	{
		case Enum::WINDOW_0:
			break;
		case Enum::WINDOW_1:
			break;
		case Enum::WINDOW_2:
			spr[0]->Render2(GetDeviceContext(), 0, 0, 0, 0, 1520, 1280, static_cast<float>(WinFunc::GetScreenWidth(i)) / 1520, static_cast<float>(WinFunc::GetScreenHeight(i)) / 1280);
			break;
		default:
			break;
	}
}

//******************************************************************************