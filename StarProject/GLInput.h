#ifndef _GLINPUT_H_
#define	_GLINPUT_H_
//******************************************************************************
//
//
//		入力関連処理
//
//
//******************************************************************************
//==============================================================================
//
//		入力処理
//
//==============================================================================

// 入力デバイスの設定
void SetDevice(DEVICE_ASSIGN*** pppDevice);

// 入力情報の更新
void Input();

// 入力ワークのアドレス取得
PAD_WORK* GetPadWork(int i = 0);

#define pad_w			(GetPadWork(0))

#define pad_state		(pad_w->state)
#define pad_trg			(pad_w->trg)
#define pad_volumeX		(pad_w->volume[0])
#define pad_volumeY		(pad_w->volume[1])
#define pad_volumeZ		(pad_w->volume[2])
#define pad_volumeRZ	(pad_w->volume[3])

#define pad_state1		(pad_w[0].state)
#define pad_trg1		(pad_w[0].trg)

#define pad_state2		(pad_w[1].state)
#define pad_trg2		(pad_w[1].trg)

#define pad_state3		(pad_w[2].state)
#define pad_trg3		(pad_w[2].trg)

#define pad_state4		(pad_w[3].state)
#define pad_trg4		(pad_w[3].trg)

namespace GLInput
{
	HRESULT Init(HWND hWnd);
	void Uninit();
	void Update();

	void SetDevice(DEVICE_ASSIGN*** pppDevice);
	PAD_WORK* GetPadWork(int i = 0);
};

//******************************************************************************
#endif // !_GLINPUT_H_