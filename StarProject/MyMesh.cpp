#include "MyMesh.h"
#include	"3DLib\\Wrapped.h"
#include "BaseLib\\Template.h"
#include "Stage.h"
#include "Camera.h"




//	情報初期値
void MyMesh::Initialize()
{
	pos = DirectX::XMFLOAT3(0, 0, 0);
	angle = DirectX::XMFLOAT3(0, 0, 0);
	scale = DirectX::XMFLOAT3(1, 1, 1);
	color = DirectX::XMFLOAT4(1, 1, 1, 1);


	//	モデル読み込み
	obj = nullptr;
}

//	モデル読み込み
bool MyMesh::Load(const char * fbx_filename)
{
	obj = new Skinned_mesh(GetDevice(), fbx_filename);
	bLoad = true;
	return	true;
}


// QUESTION /////////////////////////////////////////
//	メッシュモデル設定
void MyMesh::SetPrimitive(Skinned_mesh * primitive)
{
	obj = primitive;
	bLoad = true;
}

//	メッシュモデル設定(使い回し型)
void MyMesh::SetMesh(MyMesh & org)
{
	*this = org;
	bLoad = false;
}
//? ///////////////////////////////////////////////////



//	メッシュ解放
void MyMesh::Release()
{
	if (obj && bLoad)
	{
		delete	obj;
	}
	obj = nullptr;
	bLoad = false;
}











//	メッシュ描画
void MyMesh::Render(const DirectX::XMMATRIX & view, const DirectX::XMMATRIX & projection, float elapsed_time)
{
	//	モデルが無ければ描画処理を一切行わない
	if (!obj)		return;


	//	ワールド変換行列取得
	DirectX::XMMATRIX worldM = GetWorldMatrix();

	//	Matrix -> Float4x4 変換
	DirectX::XMFLOAT4X4 world_view_projection;
	DirectX::XMFLOAT4X4 world_;
	DirectX::XMStoreFloat4x4(&world_view_projection, worldM * view * projection);
	DirectX::XMStoreFloat4x4(&world_, worldM);

	//	描画
	obj->render(GetDeviceContext(), world_view_projection, world_, elapsed_time, color,
		pStageManager->Getlight_direction(), pCameraManager->GetCameraPos(), pStageManager->GetlightColor(), pStageManager->GetnyutoralLightColor());
}









//
//bool MyMesh::StanbyPresant(const DirectX::XMMATRIX & view, const DirectX::XMMATRIX & projection, float elapsed_time)
//{
//	HRESULT hr = S_OK;
//	// スタンバイモード
//	if (standByMode)
//	{
//		hr = GetSwapChain()->Present(0, DXGI_PRESENT_TEST);
//		if (hr == DXGI_STATUS_OCCLUDED)// 描画する必要がない(表示領域がない)
//		{
//			return standByMode;
//		}
//		standByMode = false;//スタンバイ・モードを解除
//	}
//	// 画面の更新
//	Render(view, projection, elapsed_time);
//	// レンダリングされたイメージをユーザーに表示します。
//	hr = GetSwapChain()->Present(0, 0);
//	if (hr == DXGI_STATUS_OCCLUDED)// 描画する必要がない(表示領域がない)
//	{
//		standByMode = true;//スタンバイ・モードに入る
//	}
//	return standByMode;
//}














//	メッシュ情報からワールド変換行列取得
DirectX::XMMATRIX MyMesh::GetWorldMatrix()
{
	DirectX::XMMATRIX world;
	DirectX::XMMATRIX S, R, Rx, Ry, Rz, T;

	//	初期化
	//world = DirectX::XMMatrixIdentity();


	//	拡大・縮小
	S = DirectX::XMMatrixScaling(scale.x, scale.y, scale.z);
	//	回転
	Rx = DirectX::XMMatrixRotationX(angle.x);
	Ry = DirectX::XMMatrixRotationY(angle.y);
	Rz = DirectX::XMMatrixRotationZ(angle.z);
	R = Rz * Ry * Rx;
	//	平行移動
	T = DirectX::XMMatrixTranslation(pos.x, pos.y, pos.z);

	//	ワールド変換行列
	world = S * R * T;

	return world;
}
