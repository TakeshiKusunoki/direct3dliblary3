#include "All.h"

//==============================================================================
//
//      OBJ3Dクラス
//
//==============================================================================
OBJ3D::OBJ3D()
{
	clear();
	scale = { 1,1,1 };
	color = { 1,1,1,1 };
}

void OBJ3D::clear()
{
	mvAlg = nullptr;              // 移動アルゴリズム
	eraseAlg = nullptr;           // 消去アルゴリズム

	SecureZeroMemory(&Model, sizeof(Model));
	SecureZeroMemory(&Anm, sizeof(Anm));
	position = {};//位置
	scale = {};//大きさ
	angle = {};//回転角度
	color = {};//色
	windowNum = 0;
	playerNum = 0;

	state = 0;
	timer = 0;

	size = { 0 ,0 ,0 };

	// ローカル構造体---------------------------------
	Flag = { false };
	Behavier = { nullptr };
	stateMethodWait = { 0 };
	stateMethodWalk = { 0 };
	stateMethodAttack = { 0 };
	GameMethod = { 0 };
	SecureZeroMemory(&Axcel, sizeof(Axcel));
	SecureZeroMemory(&CharInfo, sizeof(CharInfo));
	SecureZeroMemory(&ShotInfo, sizeof(ShotInfo));
	Command = { false };
	PrevCommand = { false };
	// -----------------------------------------------
	// 委譲
	actorState = nullptr;
	// マネージャー
	if (helperManager.getList())helperManager.init();
}



void OBJ3D::move()
{
	if (mvAlg) mvAlg->move(this);        // 移動アルゴリズムが存在すれば、moveメソッドを呼ぶ
	if (eraseAlg) eraseAlg->erase(this); // 消去アルゴリズムが存在すれば、eraseメソッドを呼ぶ
	//helperManager
	if (helperManager.getList())helperManager.update(this);
}

void OBJ3D::draw(const DirectX::XMMATRIX & view, const DirectX::XMMATRIX & projection, float elapsed_time)
{
	if(Model.obj)Model.Render(view, projection, elapsed_time);
	if (helperManager.getList())helperManager.draw(view, projection, elapsed_time);
}





//void OBJ3D::drawHitRect(const VECTOR4 &)
//{
//}













//==============================================================================
//
//      消去アルゴリズム
//
//==============================================================================

void EraceObj::erase(OBJ3D * obj)
{
	printf("obj->position.x%f", obj->position.x);
	printf("obj->position.y%f", obj->position.y);
	printf("obj->position.z%f\n", obj->position.z);

	if (obj->helperManager.getList())obj->helperManager.UnInit();
	obj->clear();
}












//==============================================================================
//
// OBJ3DManagerクラス
//
//==============================================================================

void OBJ3DManager::init()
{
	//リストのOBJ3Dを全てクリアする
	objList.clear();
}

void OBJ3DManager::update()
{
	// 移動
	for (auto& it : objList)            // objListの全ての要素をループし、itという名前で各要素にアクセス
	{
		it.move();                      // itのmoveメソッドを呼ぶ
	}

	// 空ノードの削除（今は気にしなくて良い）
	auto it = objList.begin();
	while (it != objList.end())
	{
		if (it->mvAlg)
		{
			it++;
		}
		else
		{
			it = objList.erase(it);
		}
	}
}

void OBJ3DManager::draw(const DirectX::XMMATRIX & view, const DirectX::XMMATRIX & projection, float elapsed_time)
{
	for (auto& it : objList)            // objListの全ての要素をループし、itという名前で各要素にアクセス
	{
		it.draw(view, projection, elapsed_time);                      // itのdrawメソッドを呼ぶ
	}
}




//object追加
OBJ3D * OBJ3DManager::add(MoveAlg * mvAlg, MoveAlgHelper * mvAlgHelper[], DirectX::XMFLOAT3 position, UINT windowNum_, int playerNum_, bool isAI ,DirectX::XMFLOAT3 scale, DirectX::XMFLOAT3 angle, DirectX::XMFLOAT4 color)
{
	OBJ3D obj;
	obj.mvAlg = mvAlg;
	//mvAlgHelperは最後にNULLの入った配列
	for (UINT i = 0; mvAlgHelper[i] != nullptr ; i++)
	{
		obj.helperManager.add(mvAlgHelper[i], position);
	}

	obj.position = position;
	obj.scale = scale;
	obj.angle.x = angle.x;
	obj.angle.y = angle.y;
	obj.angle.z = angle.z;

	obj.color = color;
	obj.windowNum = windowNum_;
	obj.playerNum = playerNum_;
	obj.Flag.AI = isAI;
	objList.push_back(obj);//リストに追加
	return &(*objList.rbegin());
}



